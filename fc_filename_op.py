'''
cd /home/dev/family-compo-asset/family-compo-asset-utility
python3 ./fc_scale_image.py 


'''
import errno
import glob
import os
import shutil
import sys

import util_logging
import util_common

# let me import fc_tag
script_dir = os.path.dirname(os.path.realpath(os.path.join(os.getcwd(), os.path.expanduser(__file__)) ) )
sys.path.append(os.path.normpath(os.path.join(script_dir, '../family-compo-asset-vol01')))
print('sys.path=', sys.path)
import fc_tag

def glob_filter(source_dir, include_filters, exclude_filters=None):
    '''
    glob_filter('/a/b/c', '/d/e/f', ['*.h'], [])
    glob_filter('/a/b/c', '/d/e/f', ['*.h', '*.py'], [])
    glob_filter('/a/b/c', '/d/e/f', ['*'], '*.pyc')
    glob_filter('/a/b/c', '/d/e/f', '*', ['*.pyc', '*.log'])
    '''
    if include_filters is None:
        include_filters = []

    if exclude_filters is None:
        exclude_filters = []

    include_files = []
    for filter_ in include_filters:
         include_files += glob.iglob(os.path.join(source_dir, filter_))
    #log('include_files=%s', ' '.join(include_files))

    exclude_files = []
    for filter_ in exclude_filters:
        exclude_files += glob.iglob(os.path.join(source_dir, filter_))
    #log('exclude_files=%s', ' '.join(exclude_files))

    files = list(set(include_files) - set(exclude_files))
    #log('files=%s', ' '.join(files))
    return files

def get_immediate_subdirs(a_dir):
    sub_archives = []
    try:
        sub_archives = os.listdir(a_dir)
    except OSError as e:
        context = ' os.listdir(%s). '%a_dir
        if e.errno == errno.ENOENT:
            raise RuntimeError('No such file or directory. ' + context)
        else:
            raise RuntimeError('Unhandled exception: %s, %s: (%s).', errno.errorcode[e.errno], e.strerror, context)

    return [name for name in sub_archives if os.path.isdir(a_dir+'/'+name)]

#
def test_gather_files(root_dir, include_filters, exclude_filters):
    util_logging.info('>> test_gather_files(%s, )', root_dir)

    root_dir = root_dir.replace('\\','/')
    
    if root_dir[-1] != '/':
        root_dir += '/'
    
    filepath_list = glob_filter(root_dir, include_filters, exclude_filters)
    filepath_list.sort()

    util_logging.info('>> %d found:', len(filepath_list))
    #for filepath in filepath_list:
    #    util_logging.info('%s', filepath)
    util_logging.info('<< %d found.', len(filepath_list))
    #util_logging.info('\n')
    #for filepath in filepath_list:
    #    do_statistics(filepath)

    # process subdirectories
    #subdirs = get_immediate_subdirs(root_dir)
    filepath_list_in_subdirectory = []
    sub_archive_list = glob.glob(os.path.join(root_dir, '*'))
    for sub_archive in sub_archive_list:
        if sub_archive in [os.path.join(root_dir, exclude_filter) for exclude_filter in exclude_filters ]:
            util_logging.info('exclude: %s', sub_archive)
            continue
        if os.path.isdir(sub_archive):
            filepath_list_in_subdirectory += test_gather_files(sub_archive, include_filters, exclude_filters)
            
    return list(set(filepath_list + filepath_list_in_subdirectory))

#
def print_list(list_data):
    util_logging.info('print list:')
    util_logging.info('>> length: %d', len(list_data))
    for filepath in list_data:
        util_logging.info('%s', filepath)
    util_logging.info('<< length: %d', len(list_data))
    util_logging.info('\n')

#
def on_filename_1(prefix, basename, postfix):  
    '''
    \@in  '![](img/', '12_003_0__.jpg', ')'
    \@ret '![](img/12_003_0__.jpg)'
    '''
    return prefix + basename + postfix
    
def on_filename_2(prefix, basename, postfix):  
    '''
    \@in  '![](img/', '12_003_0__.jpg', ')'
    \ret  '- 12_003_0:  '\
          '![](img/12_003_0__.jpg)  \n'
    '''
    shot_name = basename.split('__')[0]
    return '- %s:  \n'%(shot_name) + \
           prefix + basename + postfix +'  \n'
           
def on_filename_3(prefix, basename, postfix):  
    '''
    \@in  '![](img/', '12_003_0__.jpg', ')'
    \ret  '### 12_003_0:  {#12_003_0}  '\
          '![](img/12_003_0__.jpg)  \n'
    '''
    shot_name = basename.split('__')[0]
    return '### %s:  {#%s}  \n'%(shot_name, shot_name) + \
           prefix + basename + postfix +'  \n'
    
#
def do_filename_op(filepath_list, prefix='', postfix=''):
    util_logging.info('do_filename_op(filepath_list, prefix=%s, postfix=%s)'%(prefix, postfix))
    
    util_logging.info('\n')
    ret_list = []
    #print_list(my_list)
    for filepath in filepath_list:
        basename = os.path.basename(filepath)
        #str_ = on_filename_1(prefix, basename, postfix)
        str_ = on_filename_2(prefix, basename, postfix)
        #str_ = on_filename_3(prefix, basename, postfix)
        print(str_)
        ret_list.append(str_)
        
    #print_list(ret_list)
    return ret_list


def test_rename(src_dir, filepath_list ):
    src_dir = src_dir.replace('\\','/')
    
    if src_dir[-1] != '/':
        src_dir += '/'
        
        
    src_filepath_list = filepath_list
    src_filepath_list.sort()

    util_logging.info('> %d found:', len(src_filepath_list))
    for src_filepath in src_filepath_list:
        util_logging.info('%s', src_filepath)
    util_logging.info('< %d found.', len(src_filepath_list))
    util_logging.info('\n')

    
    i = 0
    # for each file ...
    for src_filepath in src_filepath_list:
        src_file = os.path.basename(src_filepath)
        util_logging.info('%s', src_file)
        
        ext = src_file.split('.')[-1]
        tmp_array = src_file.split('__')
        #util_logging.info('debug=<%s>', tmp_array)        
        assert len(tmp_array) > 0
        assert len(tmp_array[0]) == 8 # e.g. 07_001_0
        
        # 
        dst_file = tmp_array[0]+'__.'+ext
        
        util_logging.info('rename: %s', src_dir+src_file)
        util_logging.info('    to: %s', src_dir+dst_file)
        

        try:
            ''' # Maybe, it doesn't need to test the file existence
            if os.path.exists(src_dir+dst_file):
                util_logging.warning('>> destination file already exists. <%s>', src_dir+dst_file)
                #raise 
            '''
            os.rename(src_dir+src_file, src_dir+dst_file)
        except OSError as e:  
            util_logging.error('>> <%s>', e)
            raise
        except:
            util_logging.fatal('Unexcepted error <%s, %s>'%(src_file, dst_file))
            raise RuntimeError('How can we get here?')
            
        i += 1
        util_logging.info('%d/%d processed.\n', i, len(src_filepath_list)) 
        
       

print('Start >>>')

os.environ['AUTOMATION_LOG_LEVEL'] = 'INFO'
util_logging.setup()

username = os.path.expanduser('~').split('/')[-1]

root_dir='/media/%s/Traveller/usb/tmp/tmp_rename/'%(username)
#root_dir='/media/%s/home/tmp'%(username),

util_logging.info('rename begin----------------')
filepath_list = test_gather_files(
root_dir=root_dir,
include_filters=['*.jpg', '*.png'],
exclude_filters=[]
)
filepath_list.sort()
#print_list(filepath_list)

test_rename(
src_dir=root_dir, 
filepath_list=filepath_list
)

util_logging.info('rename end ----------------')

filepath_list = test_gather_files(
root_dir=root_dir,
include_filters=['*.jpg', '*.png'],
exclude_filters=[]
)
filepath_list.sort()
#print_list(filepath_list)

'''
filename_op_list = do_filename_op(
filepath_list, 
prefix='![](img/', 
postfix=')'
)
'''
#filename_op_list.sort()
#print_list(filename_op_list)
#print('%d found, %d processed'%(len(filepath_list), len(filename_op_list)))
print('<<< End')

