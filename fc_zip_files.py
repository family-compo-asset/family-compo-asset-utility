'''
cd /home/dev/family-compo-asset/family-compo-asset-utility
python3 ./fc_scale_image.py 


'''
import errno
import glob
import os
import shutil
import sys

import util_logging
import util_common

# let me import fc_tag
script_dir = os.path.dirname(os.path.realpath(os.path.join(os.getcwd(), os.path.expanduser(__file__)) ) )
sys.path.append(os.path.normpath(os.path.join(script_dir, '../family-compo-asset-vol01')))
print('sys.path=', sys.path)
import fc_tag

def glob_filter(source_dir, include_filters, exclude_filters=None):
    '''
    glob_filter('/a/b/c', '/d/e/f', ['*.h'], [])
    glob_filter('/a/b/c', '/d/e/f', ['*.h', '*.py'], [])
    glob_filter('/a/b/c', '/d/e/f', ['*'], '*.pyc')
    glob_filter('/a/b/c', '/d/e/f', '*', ['*.pyc', '*.log'])
    '''
    if include_filters is None:
        include_filters = []

    if exclude_filters is None:
        exclude_filters = []

    include_files = []
    for filter_ in include_filters:
         include_files += glob.iglob(os.path.join(source_dir, filter_))
    #log('include_files=%s', ' '.join(include_files))

    exclude_files = []
    for filter_ in exclude_filters:
        exclude_files += glob.iglob(os.path.join(source_dir, filter_))
    #log('exclude_files=%s', ' '.join(exclude_files))

    files = list(set(include_files) - set(exclude_files))
    #log('files=%s', ' '.join(files))
    return files

def get_immediate_subdirs(a_dir):
    sub_archives = []
    try:
        sub_archives = os.listdir(a_dir)
    except OSError as e:
        context = ' os.listdir(%s). '%a_dir
        if e.errno == errno.ENOENT:
            raise RuntimeError('No such file or directory. ' + context)
        else:
            raise RuntimeError('Unhandled exception: %s, %s: (%s).', errno.errorcode[e.errno], e.strerror, context)

    return [name for name in sub_archives if os.path.isdir(a_dir+'/'+name)]

#
def test_gather_files(root_dir, include_filters, exclude_filters):
    util_logging.info('>> test_tag_statistics(%s, )', root_dir)

    root_dir = root_dir.replace('\\','/')
    
    if root_dir[-1] != '/':
        root_dir += '/'
    
    filepath_list = glob_filter(root_dir, include_filters, exclude_filters)
    filepath_list.sort()

    util_logging.info('>> %d found:', len(filepath_list))
    #for filepath in filepath_list:
    #    util_logging.info('%s', filepath)
    util_logging.info('<< %d found.', len(filepath_list))
    #util_logging.info('\n')
    #for filepath in filepath_list:
    #    do_statistics(filepath)

    # process subdirectories
    #subdirs = get_immediate_subdirs(root_dir)
    filepath_list_in_subdirectory = []
    sub_archive_list = glob.glob(os.path.join(root_dir, '*'))
    for sub_archive in sub_archive_list:
        if sub_archive in [os.path.join(root_dir, exclude_filter) for exclude_filter in exclude_filters ]:
            util_logging.info('exclude: %s', sub_archive)
            continue
        if os.path.isdir(sub_archive):
            filepath_list_in_subdirectory += test_gather_files(sub_archive, include_filters, exclude_filters)
            
    return list(set(filepath_list + filepath_list_in_subdirectory))

#
def print_list(list_data):
    util_logging.info('print list:')
    util_logging.info('>> length: %d', len(list_data))
    for filepath in list_data:
        util_logging.info('%s', filepath)
    util_logging.info('<< length: %d', len(list_data))
    util_logging.info('\n')

#
def subprocess_Popen(cmd, showSTDOUTDATA=False):
    at_logging.gLogger.info('subprocess_Popen(cmd=%s)', cmd)

    #import shlex
    #args = shlex.split(cmd) # don't use shlex.split(), because it transfers 'C:\Users\Public\auto_ws\' to 'C:UsersPublicauto_ws' which is wrong!
    #log('args=<%s>', ', '.join(args))

    import subprocess
    sys.stdout.flush()
    # subproc = subprocess.Popen(cmd+' '+arg, stdout=subprocess.PIPE, stderr=subprocess.PIPE) # ERROR
    subproc = subprocess.Popen(cmd, stdout=subprocess.PIPE, stderr=subprocess.PIPE,
                               shell=True)  # close_fds=True can't accompany with PIPE on Windows

    # TODO: I comment this block temporarily since mayapy is frozen here
    # if at_logging.isLogLevel('DEBUG'):
    #     # output message in real-time
    #     while True:
    #         #at_logging.gLogger.debug('begin to subprocess.stdout.readline ...')
    #         nextline = subproc.stdout.readline()  # It's weird that mayapy is frozen here
    #         #at_logging.gLogger.debug('subprocess.stdout.readline=<%s>'%(nextline))
    #         if nextline == '' and subproc.poll() is not None:
    #             break
    #         sys.stdout.write(trimStdoutputString(nextline))
    #         sys.stdout.flush()

    (STDOUTDATA, STDERRDATA) = subproc.communicate()

    retcode = subproc.poll()
    at_logging.gLogger.debug('return code:<%d>', retcode)

    if at_logging.isLogLevel('CRITICAL') or at_logging.isLogLevel('ERROR') or \
       at_logging.isLogLevel('WARNING')  or at_logging.isLogLevel('INFO'):
        STDOUTDATA = trimStdoutputString(STDOUTDATA)
        STDOUTDATA = trimStdoutputString(STDERRDATA)

    if showSTDOUTDATA:
        printCmdOutputMsg(STDOUTDATA, STDERRDATA)

    if retcode:
        printCmdOutputMsg(STDOUTDATA, STDERRDATA)
        raise RuntimeError('Command returned non-zero exit status <%d>. Please see the error message in the above "Stderr Data" section. The command is <%s>' % (retcode, cmd))
    elif 'Failed to execute userSetup.py' in STDERRDATA:
        printCmdOutputMsg(STDOUTDATA, STDERRDATA)
        raise RuntimeError('There is an error although the command returned 0 exit status, please see the error messages in the above "Stderr Data" section. The command is <%s>' % (cmd))

    return retcode

def subprocess_call(cmd):
    at_logging.info('subprocess_call(cmd=%s)', cmd)

    import subprocess

    try:
        sys.stdout.flush()
        retcode = subprocess.call(cmd, shell=True)
        if retcode < 0:
            at_logging.warn('%s, Child was terminated by signal %d', sys.stderr, retcode)
        else:
            at_logging.debug('%s, Child returned', sys.stderr)
    except OSError as e:
        at_logging.fatal('%s, Exection failed: (%d, %s, %s)', sys.stderr, e.errno, e.strerror, e.filename)

    return retcode

def os_system(cmd):
    '''
    if at_common.os_system(cmd) != 0:
        raise Exception(self.mModuleName)
    '''
    ret = os.system(cmd)
    # ret = subprocess_Popen(cmd)
    # ret = subprocess_call(cmd)

    return ret

#
def do_zip_files(src_filepath_list, dst_dir):
    util_logging.info('\n')
    dst_dir = dst_dir.replace('\\','/')
    
    if dst_dir[-1] != '/':
        dst_dir += '/'
    
    util_logging.info('> %d found:', len(src_filepath_list))
    for src_filepath in src_filepath_list:
        util_logging.info('%s', src_filepath)
    util_logging.info('< %d found.', len(src_filepath_list))
    util_logging.info('\n')
    
    ret = []
    for src_filepath in src_filepath_list:
        src_basename = os.path.basename(src_filepath).split('.')[0]
        #util_logging.info('%s', src_basename)
        
        dst_filepath = dst_dir+src_basename+'.7z'
        
        cmd = '7z a -t7z -mx=9 -bb0 %s %s'%(dst_filepath, src_filepath)
        #util_logging.info('> cmd:%s', cmd) 
        if os_system(cmd) != 0:
            raise Exception(self.mModuleName)
        ret.append(dst_filepath)
        
    return ret
            
            
#
def test_zip_files(filepath_list):
    for filepath in filepath_list:
        cmd = '7z t -bb0 %s'%(filepath)
        #util_logging.info('> cmd:%s', cmd) 
        if os_system(cmd) != 0:
            raise Exception(self.mModuleName)



print('Start >>>')

os.environ['AUTOMATION_LOG_LEVEL'] = 'INFO'
util_logging.setup()

username = os.path.expanduser('~').split('/')[-1]
root_dir='/media/%s/Traveller/usb/tmp/tmp_zip/'%(username)
#root_dir='/media/%s/home/tmp'%(username),
src_dir=root_dir+'from/'
dst_dir=root_dir+'to/'

src_filepath_list = test_gather_files(
src_dir,
include_filters=['*.xcf'],
exclude_filters=['family-compo-asset-utility', 'family-compo-mods', '.git']
)
src_filepath_list.sort()
#print_list(src_filepath_list)

print('Zip >>>')
dst_filepath_list = do_zip_files(src_filepath_list, dst_dir)
dst_filepath_list.sort()
print_list(dst_filepath_list)

print('Test >>>')
test_zip_files(dst_filepath_list)  

print('<<< End')

