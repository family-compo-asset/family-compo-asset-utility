'''
cd /home/dev/family-compo-asset/family-compo-asset-utility
python3 ./fc_tag_analysis.py 

'''
import errno
import glob
import os
import shutil
import sys

import util_logging
import util_common

import fc_tag_class2

# let me import fc_tag
script_dir = os.path.dirname(os.path.realpath(os.path.join(os.getcwd(), os.path.expanduser(__file__)) ) )
sys.path.append(os.path.normpath(os.path.join(script_dir, '../family-compo-asset-vol01')))
print('sys.path=', sys.path)
import fc_tag



def glob_filter(source_dir, include_filters, exclude_filters=None):
    '''
    glob_filter('/a/b/c', '/d/e/f', ['*.h'], [])
    glob_filter('/a/b/c', '/d/e/f', ['*.h', '*.py'], [])
    glob_filter('/a/b/c', '/d/e/f', ['*'], '*.pyc')
    glob_filter('/a/b/c', '/d/e/f', '*', ['*.pyc', '*.log'])
    '''
    if include_filters is None:
        include_filters = []

    if exclude_filters is None:
        exclude_filters = []

    include_files = []
    for filter_ in include_filters:
         include_files += glob.iglob(os.path.join(source_dir, filter_))
    #log('include_files=%s', ' '.join(include_files))

    exclude_files = []
    for filter_ in exclude_filters:
        exclude_files += glob.iglob(os.path.join(source_dir, filter_))
    #log('exclude_files=%s', ' '.join(exclude_files))

    files = list(set(include_files) - set(exclude_files))
    #log('files=%s', ' '.join(files))
    return files

def get_immediate_subdirs(a_dir):
    sub_archives = []
    try:
        sub_archives = os.listdir(a_dir)
    except OSError as e:
        context = ' os.listdir(%s). '%a_dir
        if e.errno == errno.ENOENT:
            raise RuntimeError('No such file or directory. ' + context)
        else:
            raise RuntimeError('Unhandled exception: %s, %s: (%s).', errno.errorcode[e.errno], e.strerror, context)

    return [name for name in sub_archives if os.path.isdir(a_dir+'/'+name)]


def print_list(lst, msg, to_show_details):
    util_logging.info('>> <%s>, %d found:', msg, len(lst))
    
    if to_show_details:# not only show the list length, but also show the list content
        for f in lst:
            util_logging.info('%s', f)
        
    util_logging.info('<< <%s>, %d found.', msg, len(lst))
    util_logging.info('\n')


def gather_files(ret_list, root_dir, include_filters, exclude_filters):
    '''
    gather files under directory: \@root_dir
    '''
    util_logging.info('>> gather_files(, %s, )', root_dir)

    root_dir = root_dir.replace('\\','/')
    
    if root_dir[-1] != '/':
        root_dir += '/'
    
    filepath_list = glob_filter(root_dir, include_filters, exclude_filters)
    filepath_list.sort()

    print_list(filepath_list, '', to_show_details = False)
    
    ret_list += filepath_list

    # process subdirectories
    #subdirs = get_immediate_subdirs(root_dir)
    sub_archive_list = glob.glob(os.path.join(root_dir, '*'))
    for sub_archive in sub_archive_list:
        if sub_archive in [os.path.join(root_dir, exclude_filter) for exclude_filter in exclude_filters ]:
            util_logging.info('exclude: %s', sub_archive)
            continue
        if os.path.isdir(sub_archive):
            gather_files(ret_list, sub_archive, include_filters, exclude_filters)


def dict_to_string(d, msg):
    ret_str = ''
    
    ret_str += msg
    
    import operator 
    cd = sorted(d.items(), key=operator.itemgetter(1), reverse=True )
    
    import pprint    
    # 1. output without a format
    #print('pprint.pprint-----------------')
    #pprint.pprint(cd) 
    
    # 2. output with a format
    # 2.1 with pformat
    # output to a string with pprint format
    ret_str += pprint.pformat(cd) 
    # 2.2 with json format
    #import json
    #ret_str += json.dumps(cd, indent=4)
        
    return ret_str


def output_statistics_result(file_path=''):
    util_logging.info('>> output_statistics_result(%s, )', file_path)

    file_path = file_path.replace('\\','/')
        
    str_o = '' # string to be output
    
    str_o += dict_to_string(fc_tag.name, '\nname-------------------------\n')
    str_o += dict_to_string(fc_tag.direction, '\ndirection-------------------------\n')
    str_o += dict_to_string(fc_tag.expression, '\nexpression-------------------------\n')
    str_o += dict_to_string(fc_tag.action, '\naction-------------------------\n')
    str_o += dict_to_string(fc_tag.dress, '\ndress-------------------------\n')
    str_o += dict_to_string(fc_tag.extension, '\nextension-------------------------\n')
    str_o += dict_to_string(fc_tag.scene, '\nscene-------------------------\n')
    str_o += dict_to_string(fc_tag.prop, '\nprop-------------------------\n')
    str_o += dict_to_string(fc_tag.effect, '\neffect-------------------------\n')

    print('STATISTICS RESULT-----------------')
    
    # output to the terminal
    #print(str_o)  
    
    # output to a file
    with open(file_path, 'w') as f:
        f.write(str_o)


def check_file_name_ext(filepath_list):
    '''
    check file name extension
    '''
    for filepath in filepath_list:
        fn_base = os.path.basename(filepath)
        #util_logging.info('fn_base: %s', fn_base)

        dot_group = fn_base.split('.')
        #util_logging.info('dot_group: %s', str(dot_group))
        assert len(dot_group) == 2, 'len(dot_group)=<%d>, file: <%s>'%(len(dot_group), fn_base)
        
        fn_no_ext = dot_group[0]
        #util_logging.info('fn_no_ext: %s', fn_no_ext)
        
        ext = fn_base.split('.')[-1]
        #util_logging.info('ext: %s', ext)
        assert ext == 'jpg' or ext == 'png', 'unexcepted ext: <%s>, file: <%s>'%(ext, fn_base)
        
        #util_logging.info('\n')
        

def main(root_dir, include_filters, exclude_filters):
    print('Check File Type. Start >>>')
    
    volume_list = [
    '01', '02', '03', '04', '05', '06', '07', 
    '08', '09', '10', '11', '12', '13', '14']
    for vi in volume_list:
        dir2 = root_dir+'family-compo-asset-vol%s/v%s'%(vi, vi)
        
        g_filelist = []
        
        gather_files(
        g_filelist,
        root_dir=dir2,
        include_filters=include_filters,
        exclude_filters=exclude_filters
        )
        print_list(g_filelist, 'dir=%s'%(dir2), to_show_details = False)
        
        check_file_name_ext(g_filelist)
    
    #output_statistics_result(file_path='/media/%s/home/tmp4/output/fc_tag_statistics.txt'%(username))
    print('<<< End. Check File Type')
    

if '__main__' == __name__:

    os.environ['AUTOMATION_LOG_LEVEL'] = 'INFO'
    util_logging.setup()

    username = os.path.expanduser('~').split('/')[-1]


    main(
    #root_dir='/media/%s/home/tmp5/'%(username),
    root_dir='/media/%s/home/dev/family-compo-asset/'%(username),
    #root_dir='/media/%s/home/tmp'%(username),
    include_filters=['*'],
    exclude_filters=['family-compo-asset-utility', 'family-compo-mods', '.git']
    )



