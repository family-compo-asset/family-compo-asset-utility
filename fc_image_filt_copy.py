'''
cd /home/dev/family-compo-asset/family-compo-asset-utility
python3 ./fc_image_filt_copy.py 


'''
import errno
import glob
import os
import shutil
import sys

import util_logging
import util_common

# let me import fc_tag
script_dir = os.path.dirname(os.path.realpath(os.path.join(os.getcwd(), os.path.expanduser(__file__)) ) )
sys.path.append(os.path.normpath(os.path.join(script_dir, '../family-compo-asset-vol01')))
print('sys.path=', sys.path)
import fc_tag

def glob_filter(source_dir, include_filters, exclude_filters=None):
    '''
    glob_filter('/a/b/c', '/d/e/f', ['*.h'], [])
    glob_filter('/a/b/c', '/d/e/f', ['*.h', '*.py'], [])
    glob_filter('/a/b/c', '/d/e/f', ['*'], '*.pyc')
    glob_filter('/a/b/c', '/d/e/f', '*', ['*.pyc', '*.log'])
    '''
    if include_filters is None:
        include_filters = []

    if exclude_filters is None:
        exclude_filters = []

    include_files = []
    for filter_ in include_filters:
         include_files += glob.iglob(os.path.join(source_dir, filter_))
    #log('include_files=%s', ' '.join(include_files))

    exclude_files = []
    for filter_ in exclude_filters:
        exclude_files += glob.iglob(os.path.join(source_dir, filter_))
    #log('exclude_files=%s', ' '.join(exclude_files))

    files = list(set(include_files) - set(exclude_files))
    #log('files=%s', ' '.join(files))
    return files

def get_immediate_subdirs(a_dir):
    sub_archives = []
    try:
        sub_archives = os.listdir(a_dir)
    except OSError as e:
        context = ' os.listdir(%s). '%a_dir
        if e.errno == errno.ENOENT:
            raise RuntimeError('No such file or directory. ' + context)
        else:
            raise RuntimeError('Unhandled exception: %s, %s: (%s).', errno.errorcode[e.errno], e.strerror, context)

    return [name for name in sub_archives if os.path.isdir(a_dir+'/'+name)]

#
def test_gather_files(root_dir, include_filters, exclude_filters):
    util_logging.info('>> test_tag_statistics(%s, )', root_dir)

    root_dir = root_dir.replace('\\','/')
    
    if root_dir[-1] != '/':
        root_dir += '/'
    
    filepath_list = glob_filter(root_dir, include_filters, exclude_filters)
    filepath_list.sort()

    util_logging.info('>> %d found:', len(filepath_list))
    #for filepath in filepath_list:
    #    util_logging.info('%s', filepath)
    util_logging.info('<< %d found.', len(filepath_list))
    #util_logging.info('\n')
    #for filepath in filepath_list:
    #    do_statistics(filepath)

    # process subdirectories
    #subdirs = get_immediate_subdirs(root_dir)
    filepath_list_in_subdirectory = []
    sub_archive_list = glob.glob(os.path.join(root_dir, '*'))
    for sub_archive in sub_archive_list:
        if sub_archive in [os.path.join(root_dir, exclude_filter) for exclude_filter in exclude_filters ]:
            util_logging.info('exclude: %s', sub_archive)
            continue
        if os.path.isdir(sub_archive):
            filepath_list_in_subdirectory += test_gather_files(sub_archive, include_filters, exclude_filters)
            
    return list(set(filepath_list + filepath_list_in_subdirectory))

#
def print_list(list_data):
    util_logging.info('print list:')
    util_logging.info('>> length: %d', len(list_data))
    for filepath in list_data:
        util_logging.info('%s', filepath)
    util_logging.info('<< length: %d', len(list_data))
    util_logging.info('\n')

#
def do_filt(filter_func, filepath_list, height=None, width=None):
    util_logging.info('\n')
    
    ret_list = []
    for filepath in filepath_list:
        if is_filted(filter_func, input_image_path=filepath, height=height, width=width):  
            ret_list.append(filepath)
            
    return ret_list

#
def is_strictly_smaller(w, h, width, height):  
    return h < height and w < width
    
def is_smaller(w, h, width, height):  
    return h < height or w < width  
    
def is_larger(w, h, width, height):  
    return h >= height and w >= width  
    
#
from PIL import Image
#print(Image.__file__)
def is_filted(filter_func, 
                input_image_path,
                height=None,
                width=None
                ):
    original_image = Image.open(input_image_path)
    w, h = original_image.size
    #print('The original image size is {wide} wide x {height} high'.format(wide=w, height=h))

    ret_filted = False 
    if filter_func(w, h, width, height):
        #print('Source image is filtered. (%s,%s) <%s>'%(w, h, input_image_path))
        ret_filted = True

    original_image.close()
    return ret_filted




print('Start >>>')

os.environ['AUTOMATION_LOG_LEVEL'] = 'INFO'
util_logging.setup()

username = os.path.expanduser('~').split('/')[-1]

root_dir='/media/%s/Traveller/usb/tmp/'%(username)

filepath_list = test_gather_files(
root_dir=root_dir+'filt_copy/',
#root_dir='/media/%s/home/tmp'%(username),
include_filters=['*.jpg', '*.png', '*.jpeg', '*.webp', '*.avif'],
exclude_filters=['family-compo-asset-utility', 'family-compo-mods', '.git']
)
filepath_list.sort()
#print_list(filepath_list)
H = 512
W = 512


dst_dir = root_dir+'strictly_smaller/'
print('\n\n\nfilter strictly smaller images with (H=%d, W=%d), then copy the filtered images to %s'%(H, W, dst_dir))
filepath_list0 = do_filt(is_strictly_smaller, filepath_list, height=H, width=W)
filepath_list0.sort()
#print_list(filepath_list0)
util_common.tryToResetDir(dst_dir)
[shutil.copy2(p, dst_dir) for p in filepath_list0]

dst_dir = root_dir+'smaller/'
print('\n\n\nfilter smaller images with (H=%d, W=%d), then copy the filtered images to %s'%(H, W, dst_dir))
filepath_list1 = do_filt(is_smaller, filepath_list, height=H, width=W)
filepath_list1.sort()
#print_list(filepath_list1)
util_common.tryToResetDir(dst_dir)
[shutil.copy2(p, dst_dir) for p in filepath_list1]

dst_dir = root_dir+'larger/'
print('\n\n\nfilter larger images with (H=%d, W=%d), then copy the filtered images to %s'%(H, W, dst_dir))
filepath_list2 = do_filt(is_larger, filepath_list, height=H, width=W)
filepath_list2.sort()
#print_list(filepath_list2)
util_common.tryToResetDir(dst_dir)
[shutil.copy2(p, dst_dir) for p in filepath_list2]

#do_move(filepath_src=filepath_list, dst_dir=root_dir='/media/%s/Traveller/usb/tmp/filted/'%(username))

print('<<< End')

