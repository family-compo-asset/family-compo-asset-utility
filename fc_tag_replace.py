'''
python3 ./fc_tag_replace.py
'''
import errno
import os
import shutil
import util_logging
import util_common

def glob_filter(source_dir, include_filters, exclude_filters=None):
    '''
    glob_filter('/a/b/c', '/d/e/f', ['*.h'], [])
    glob_filter('/a/b/c', '/d/e/f', ['*.h', '*.py'], [])
    glob_filter('/a/b/c', '/d/e/f', ['*'], '*.pyc')
    glob_filter('/a/b/c', '/d/e/f', '*', ['*.pyc', '*.log'])
    '''
    if include_filters is None:
        include_filters = []

    if exclude_filters is None:
        exclude_filters = []

    import glob, os
    include_files = []
    for filter_ in include_filters:
         include_files += glob.iglob(os.path.join(source_dir, filter_))
    #log('include_files=%s', ' '.join(include_files))

    exclude_files = []
    for filter_ in exclude_filters:
        exclude_files += glob.iglob(os.path.join(source_dir, filter_))
    #log('exclude_files=%s', ' '.join(exclude_files))

    files = list(set(include_files) - set(exclude_files))
    #log('files=%s', ' '.join(files))
    return files

def test_filter():
    files = glob_filter('/home/zz/Pictures/v01', ['*horror*.jpg'])
    files.sort()

    print('%d found:'%(len(files)))
    for f in files:
        print('%s'%(f))
    
def test_filename_longer_than_255():
    src_dir = '/media/zz/home/tmp/src/'
    #src_dir = '/home/zz/tmp/'
    dst_dir = '/media/zz/home/tmp/dst/'
    
    filename_list = os.listdir(src_dir)
    filename_list.sort()
    for filename in filename_list:
        print('on file: %s'%(filename))
        os.rename(src_dir+filename, dst_dir+'0123456789'*25+filename)
        
    '''
    filename = 'A'+'0123456789'*25+'.jpg'
    
    try:
        filepath = test_dir+'/'+filename
        with open(filepath, 'w') as f:
            print('file is open: %s'%(filepath))
            
            os.rename()
            
    except Exception as e:
        context = ''
        raise RuntimeError('Unhandled exception: %s, %s: (%s).', errno.errorcode[e.errno], e.strerror, context)
 
    filename = 'AB'+'0123456789'*25+'.jpg'
    try:
        filepath = test_dir+'/'+filename
        with open(filename, 'w') as f:
            print('file is open: %s'%(filename))
    except Exception as e:
        context = ''
        raise RuntimeError('Unhandled exception: %s, %s: (%s).', 
                errno.errorcode[e.errno], # ENAMETOOLONG
                e.strerror, # 'File name too long'
                context)
    '''
    
def test_replace_(final_act, src_dir, filter_pattern, from_str, to_str, dst_dir ):
    src_dir = src_dir.replace('\\','/')
    dst_dir = dst_dir.replace('\\','/')
    
    if src_dir[-1] != '/':
        src_dir += '/'
        
    if dst_dir[-1] != '/':
        dst_dir += '/'
    

    src_filepath_list = glob_filter(src_dir, filter_pattern)
    src_filepath_list.sort()

    util_logging.info('>> %d found:', len(src_filepath_list))
    for src_filepath in src_filepath_list:
        util_logging.info('%s', src_filepath)
    util_logging.info('<< %d found.', len(src_filepath_list))
    util_logging.info('\n')


    # clear dst directory
    util_common.tryToResetDir(dst_dir)    
    
    i = 0
    # for each file ...
    for src_filepath in src_filepath_list:
        src_file = os.path.basename(src_filepath)
        util_logging.info('%s', src_file)
        
        # replace ...
        dst_file = src_file.replace(from_str, to_str)
        
        
        if final_act:
            util_logging.info('rename: %s', src_dir+src_file)
            util_logging.info('    to: %s', src_dir+dst_file)
            os.rename(src_dir+src_file, src_dir+dst_file)
        else:
            util_logging.info('copy: %s', src_dir+src_file)
            util_logging.info('  to: %s', dst_dir)
            shutil.copy2(src_dir+src_file, dst_dir)
            
            util_logging.info('rename: %s', dst_dir+src_file)
            util_logging.info('    to: %s', dst_dir+dst_file)
            os.rename(dst_dir+src_file, dst_dir+dst_file)
        i += 1
        util_logging.info('%d/%d processed.\n', i, len(src_filepath_list)) 
        
       
        
print('Start >>>')

os.environ['AUTOMATION_LOG_LEVEL'] = 'INFO'
util_logging.setup()

#test_filename_longer_than_255()

username = os.path.expanduser('~').split('/')[-1]
test_replace_(
    final_act=True,
    # final_act=False,
    src_dir='/media/%s/home/dev/family-compo-asset/family-compo-asset-vol04/v04'%(username), 
    filter_pattern=['*-younger*.jpg'], 
    from_str='-younger',
    to_str  ='_younger',
    dst_dir='/media/%s/home/tmp/dst'%(username)
)
print('<<< End')




