'''
cd /home/dev/family-compo-asset/family-compo-asset-utility
python3 ./fc_tag_analysis.py 

'''
import errno
import glob
import os
import shutil
import sys

import util_logging
import util_common

import fc_tag_class2

# let me import fc_tag
script_dir = os.path.dirname(os.path.realpath(os.path.join(os.getcwd(), os.path.expanduser(__file__)) ) )
sys.path.append(os.path.normpath(os.path.join(script_dir, '../family-compo-asset-vol01')))
print('sys.path=', sys.path)
import fc_tag



def glob_filter(source_dir, include_filters, exclude_filters=None):
    '''
    glob_filter('/a/b/c', '/d/e/f', ['*.h'], [])
    glob_filter('/a/b/c', '/d/e/f', ['*.h', '*.py'], [])
    glob_filter('/a/b/c', '/d/e/f', ['*'], '*.pyc')
    glob_filter('/a/b/c', '/d/e/f', '*', ['*.pyc', '*.log'])
    '''
    if include_filters is None:
        include_filters = []

    if exclude_filters is None:
        exclude_filters = []

    include_files = []
    for filter_ in include_filters:
         include_files += glob.iglob(os.path.join(source_dir, filter_))
    #log('include_files=%s', ' '.join(include_files))

    exclude_files = []
    for filter_ in exclude_filters:
        exclude_files += glob.iglob(os.path.join(source_dir, filter_))
    #log('exclude_files=%s', ' '.join(exclude_files))

    files = list(set(include_files) - set(exclude_files))
    #log('files=%s', ' '.join(files))
    return files

def get_immediate_subdirs(a_dir):
    sub_archives = []
    try:
        sub_archives = os.listdir(a_dir)
    except OSError as e:
        context = ' os.listdir(%s). '%a_dir
        if e.errno == errno.ENOENT:
            raise RuntimeError('No such file or directory. ' + context)
        else:
            raise RuntimeError('Unhandled exception: %s, %s: (%s).', errno.errorcode[e.errno], e.strerror, context)

    return [name for name in sub_archives if os.path.isdir(a_dir+'/'+name)]


def print_list(lst, msg, to_show_details):
    util_logging.info('>> <%s>, %d found:', msg, len(lst))
    
    if to_show_details:# not only show the list length, but also show the list content
        for f in lst:
            util_logging.info('%s', f)
        
    util_logging.info('<< <%s>, %d found.', msg, len(lst))
    util_logging.info('\n')


def test_tag_statistics(ret_list, root_dir, include_filters, exclude_filters):
    util_logging.info('>> test_tag_statistics(, %s, )', root_dir)

    root_dir = root_dir.replace('\\','/')
    
    if root_dir[-1] != '/':
        root_dir += '/'
    
    filepath_list = glob_filter(root_dir, include_filters, exclude_filters)
    filepath_list.sort()

    print_list(filepath_list, '', to_show_details = False)
    
    ret_list += filepath_list
    '''
    # filter with the range if it is given
    if vol_p_close_open_range is not None:
        assert 2 == len(vol_p_close_open_range), 'make sure the range is valid:%s'%(str(vol_p_close_open_range))
        
        fm = fc_tag_class2.FileListManager(filepath_list)
        #fm.print_list()
        filepath_list_in_the_range = fm.filter(vol_p_close_open_range)
        print_list(filepath_list_in_the_range, 'filtered', True)
        
        for filepath in filepath_list_in_the_range:
            do_statistics_on_one_imagefile(filepath)
    '''
    # process subdirectories
    #subdirs = get_immediate_subdirs(root_dir)
    sub_archive_list = glob.glob(os.path.join(root_dir, '*'))
    for sub_archive in sub_archive_list:
        if sub_archive in [os.path.join(root_dir, exclude_filter) for exclude_filter in exclude_filters ]:
            util_logging.info('exclude: %s', sub_archive)
            continue
        if os.path.isdir(sub_archive):
            test_tag_statistics(ret_list, sub_archive, include_filters, exclude_filters)

def do_statistics_on_one_tag(tag, fn_base):
    if tag == '':
        return
        
    elif tag in fc_tag.name:
        fc_tag.name[tag] += 1
        
    elif tag in fc_tag.direction:
        fc_tag.direction[tag] += 1
        
    elif tag in fc_tag.expression:
        fc_tag.expression[tag] += 1
        
    elif tag in fc_tag.action:
        fc_tag.action[tag] += 1
        
    elif tag in fc_tag.dress:
        fc_tag.dress[tag] += 1
        
    elif tag in fc_tag.extension:
        fc_tag.extension[tag] += 1
        
    elif tag in fc_tag.scene:
        fc_tag.scene[tag] += 1
        
    elif tag in fc_tag.prop:
        fc_tag.prop[tag] += 1
        
    elif tag in fc_tag.effect:
        fc_tag.effect[tag] += 1
        
    elif '+'==tag[0]: 
        # a tag with prefix '+' means that: this image has not this tag(e.g. an expression) 
        # in the story of F.COMPO, but this image CAN BE USED FOR this tag(expression).
        
        # remove '+' and process this tag again
        do_statistics_on_one_tag(tag[1:], fn_base)
        
    elif '-' in tag:
        w_list = tag.split('-')
        for w in w_list:
            if w in fc_tag.dress:
                fc_tag.dress[w] += 1
            else:
                raise RuntimeError('(dress)tag is not found: tag=<%s>, w=<%s>, fn_base=<%s>'%(tag, w, fn_base))

    else:
        raise RuntimeError('tag is not found: tag=<%s>, fn_base=<%s>'%(tag, fn_base))
    
def do_statistics_on_one_imagefile(filepath):
    fn_base = os.path.basename(filepath)
    #util_logging.info('fn_base: %s', fn_base)

    fn_no_ext = fn_base.split('.')[0]
    #util_logging.info('fn_no_ext: %s', fn_no_ext)

    panel = fc_tag_ahalysis.Panel(fn_no_ext)
    '''
    fn_no_index = fn_no_ext.split('__')[1:]
    #util_logging.info('fn_no_index: %s', fn_no_index)

    tag_group_list = fn_no_index
    for tag_group in tag_group_list:
        tag_list = tag_group.split('_')
        for tag in tag_list:
            do_statistics_on_one_tag(tag, fn_base)
    '''
    return panel
            

def dict_to_string(d, msg):
    ret_str = ''
    
    ret_str += msg
    
    import operator 
    cd = sorted(d.items(), key=operator.itemgetter(1), reverse=True )
    
    import pprint    
    # 1. output without a format
    #print('pprint.pprint-----------------')
    #pprint.pprint(cd) 
    
    # 2. output with a format
    # 2.1 with pformat
    # output to a string with pprint format
    ret_str += pprint.pformat(cd) 
    # 2.2 with json format
    #import json
    #ret_str += json.dumps(cd, indent=4)
        
    return ret_str


def output_statistics_result(file_path=''):
    util_logging.info('>> output_statistics_result(%s, )', file_path)

    file_path = file_path.replace('\\','/')
        
    str_o = '' # string to be output
    
    str_o += dict_to_string(fc_tag.name, '\nname-------------------------\n')
    str_o += dict_to_string(fc_tag.direction, '\ndirection-------------------------\n')
    str_o += dict_to_string(fc_tag.expression, '\nexpression-------------------------\n')
    str_o += dict_to_string(fc_tag.action, '\naction-------------------------\n')
    str_o += dict_to_string(fc_tag.dress, '\ndress-------------------------\n')
    str_o += dict_to_string(fc_tag.extension, '\nextension-------------------------\n')
    str_o += dict_to_string(fc_tag.scene, '\nscene-------------------------\n')
    str_o += dict_to_string(fc_tag.prop, '\nprop-------------------------\n')
    str_o += dict_to_string(fc_tag.effect, '\neffect-------------------------\n')

    print('STATISTICS RESULT-----------------')
    
    # output to the terminal
    #print(str_o)  
    
    # output to a file
    with open(file_path, 'w') as f:
        f.write(str_o)


def check_duplication_of_tag_value():
    dict_list = [
        fc_tag.name,
        fc_tag.direction,
        fc_tag.expression,
        fc_tag.action,
        fc_tag.dress,
        fc_tag.extension,
        fc_tag.scene,
        fc_tag.prop,
        fc_tag.effect,
    ]
    L = len(dict_list)
    
    for i in range(L):
        for j in range(i):
            if i == j:
                continue
            si = set(dict_list[i].keys())
            sj = set(dict_list[j].keys())
            intersection = si.intersection(sj)
            assert len(intersection) == 0, 'In fc_tag.py, tag_types should not share same tag_value. <%d, %d> have same tag_value: %s'%(i, j, str(intersection))
    
    
def get_file_list(root_dir, vol_p_close_open_range):
    #- 
    g_filelist = []
    
    test_tag_statistics(
    g_filelist, 
    root_dir=root_dir,
    include_filters=['*.jpg', '*.png'],
    exclude_filters=['family-compo-asset-utility', 'family-compo-mods', '.git', '__pycache__']
    )
    print_list(g_filelist, 'g_filelist', to_show_details = False)
    
    #- filter with range
    todo_filelist = []
    if vol_p_close_open_range is not None:
        g_filelist_in_the_range = []    
        assert 2 == len(vol_p_close_open_range), 'make sure the range is valid:%s'%(str(vol_p_close_open_range))
        
        fm = fc_tag_class2.FileListManager(g_filelist)
        #fm.print_list()
        g_filelist_in_the_range = fm.filter(vol_p_close_open_range)
        print_list(g_filelist_in_the_range, 'filtered', to_show_details = False)
        todo_filelist = g_filelist_in_the_range
    else:
        todo_filelist = g_filelist
        
    return todo_filelist
    

def create_panel_data(todo_filelist):
    g_plmgr = fc_tag_class2.PanelListManager()    
    # process file list
    for filepath in todo_filelist:
        #do_statistics_on_one_imagefile(filepath)
        
        fn_base = os.path.basename(filepath)   # e.g. abc123.jpg
        fn_base_no_ext = fn_base.split('.')[0] # e.g. abc123
        #util_logging.info('fn_base_no_ext: %s', fn_base_no_ext)
        
        panel = fc_tag_class2.Panel(fn_base_no_ext)
        
        g_plmgr.update(panel)
        
    return g_plmgr
        

def main_(root_dir, vol_p_close_open_range):
    todo_filelist = get_file_list(root_dir, vol_p_close_open_range)    

    g_plmgr = create_panel_data(todo_filelist)
    return g_plmgr
    

def main(root_dir, vol_p_close_open_range):
    print('Start >>>')

    #-----------------
    # Pre process
    #-----------------
    #- 
    check_duplication_of_tag_value()
    #-
    to_check_file_name_extension = False
    if to_check_file_name_extension:
        import check_file_name_extension
        check_file_name_extension.main(
        root_dir=root_dir,
        include_filters=['*'],
        exclude_filters=['family-compo-asset-utility', 'family-compo-mods', '.git', '__pycache__']
        )
    
    #-----------------
    # process
    #-----------------
    g_plmgr = main_(root_dir, vol_p_close_open_range)
    
    #-----------------
    # Post process
    #-----------------
    #- 
    g_plmgr.check('Check from PanelListManager')
    g_plmgr.print('')
        
    output_statistics_result(file_path='/media/%s/home/tmp4/output/fc_tag_statistics.txt'%(username))
    print('<<< End')


if '__main__' == __name__:
    os.environ['AUTOMATION_LOG_LEVEL'] = 'INFO' #  DEBUG INFO
    util_logging.setup()

    username = os.path.expanduser('~').split('/')[-1]
    
    root_dir = '/media/%s/home/dev/family-compo-asset/family-compo-asset-vol03/v03'%(username)
    #root_dir= '/media/%s/home/tmp5/'%(username),
    #root_dir= '/media/%s/home/tmp'%(username), 
    #root_dir= '/media/%s/home/tmp4/family-compo-asset/'%(username),
    #root_dir= '/media/%s/home/tmp'%(username),   
    
    
    import check_file_name_extension
    check_file_name_extension.main(
    root_dir=root_dir,
    include_filters=['*'],
    exclude_filters=['family-compo-asset-utility', 'family-compo-mods', '.git']
    )    
    
    
    vol_p_close_open_range=['03_000', '03_020']
    #vol_p_close_open_range=['02_192_3', '02_192_4'] # 02_011_5, 02_025_0, 02_034_2, 02_035_0, 02_036_1, 02_037_0, 02_037_3, 02_039_2, 02_081_3, 02_084_4, 02_107_1, 02_148_0, 02_152_0, 02_192_3
    main(root_dir, vol_p_close_open_range)
    

