'''
- sudo apt install python3-pip

- pip3 install --prefix=/.../mypippyprefix matplotlib


- 
import sys
sys.path.insert(0, '/.../mypippyprefix/lib/python3.8/site-packages')


source: 
https://stackoverflow.com/questions/2915471/install-a-python-package-into-a-different-directory-using-pip

pip install --target /myfolder [packages]
Installs ALL packages including dependencies under /myfolder. Does not take into account that dependent packages are already installed elsewhere in Python. You will find packages from /myfolder/[package_name]. In case you have multiple Python versions, this doesn't take that into account (no Python version in package folder name).

pip install --prefix /myfolder [packages]
Checks are dependencies already installed. Will install packages into /myfolder/lib/python3.5/site-packages/[packages]

pip install --root /myfolder [packages]
Checks dependencies like --prefix but install location will be /myfolder/usr/local/lib/python3.5/site-packages/[package_name].

pip install --user [packages]
Will install packages into $HOME: /home/[USER]/.local/lib/python3.5/site-packages Python searches automatically from this .local path so you don't need to put it to your PYTHONPATH.

=> In most of the cases --user is the best option to use. In case home folder can't be used because of some reason then --prefix.

'''

import numpy as np
import matplotlib.pyplot as plt
from matplotlib.patches import Circle, PathPatch
import mpl_toolkits.mplot3d.art3d as art3d


def main(emotion_map, data_matrix):
    test_0(emotion_map, data_matrix)
    #test_patch()


def test_0(emotion_map, data_matrix):
    '''
    print(emotion_map[:, 0]) # abb.
    print(emotion_map[:, 1]) # x
    print(emotion_map[:, 2]) # y
    print(emotion_map[:, 3]) # z
    print(emotion_map[:, 4]) # radius    
    print(emotion_map[:, 5]) # rgba
    print(emotion_map[:, 6]) # emotion name 
    '''
    fig = plt.figure()
    ax = fig.add_subplot(111, projection='3d')
    
    x = emotion_map[:, 1]
    y = emotion_map[:, 2]
    z = emotion_map[:, 3]
    s = emotion_map[:, 4].astype(int)
    c = emotion_map[:, 5]
    ax.scatter(x, y, z, c=c, s=1)  # , marker='o'
    '''
    x=[min(x), max(x)]
    y=[min(x), max(x)]
    z=[0, len(data_matrix)]
    ax.scatter(x, y, z, c='b', s=1)  # , marker='o'
    '''
    # 
    for e in emotion_map:
        p = Circle((e[1], e[2]), e[4], facecolor=e[5], alpha=e[5][3])
        ax.add_patch(p)
        art3d.pathpatch_2d_to_3d(p, z=0, zdir="z")
        
    #
    x = []
    y = []
    z = []
    s = []
    c = []
    
    rc = len(data_matrix)
    for r in range(rc):
        for e in data_matrix[r]:
            if e is not None:
                e.print()
                p = Circle((e.x, e.y), e.radius, facecolor=[e.r, e.g, e.b], alpha=e.a)
                ax.add_patch(p)
                art3d.pathpatch_2d_to_3d(p, z=r, zdir="z")
                
                x.append(e.x)
                y.append(e.y)
                z.append(r)
                s.append(1)
                c.append([e.r, e.g, e.b, e.a])
    
    ax.scatter(x, y, z, c=c, s=2)
    
    x=[min(x), max(x)]
    y=[min(y), max(y)]
    z=[min(z), max(z)]
    ax.scatter(x, y, z, c='b', s=1)
    
    '''
    ppl = 7
    print('col-----%d'%( ppl) )  
    
    nd = np.array(time_people_exp)  
    time_exp = nd[:, ppl]
    print( time_exp )
    
    for i in range(len(time_exp)):
        p = Circle((100, 100), 50, facecolor='r', alpha=0.75)
        ax.add_patch(p)
        art3d.pathpatch_2d_to_3d(p, z=0+i, zdir="z")
    '''
    
    ax.set_xlabel('X--->')
    ax.set_ylabel('Y--->')
    ax.set_zlabel('Z--->')

    plt.show()
    

def test_patch():
    fig = plt.figure()
    ax = fig.add_subplot(111, projection='3d')



    ax.set_xlim(0, 1000)
    ax.set_ylim(0, 1000)
    ax.set_zlim(0, 1000)

    plt.show()



if '__main__' == __name__:
    
    main('matplotlib')
