import os
import sys

import numpy as np

script_dir = os.path.dirname(os.path.realpath(os.path.join(os.getcwd(), os.path.expanduser(__file__)) ) )
sys.path.append(os.path.normpath(os.path.join(script_dir, '../family-compo-asset-vol01')))
print('sys.path=', sys.path)
import fc_tag


#-------------------------------
class ExpMark(object):
    #key = ''
    abb = ''
    x = 0.0
    y = 0.0
    z = 0.0
    radius = 0.0
    r = 0
    g = 0
    b = 0
    a = 0
    annotation = ''
    

    def __init__(self, abb='', x=0, y=0, z=0, radius=1, rgba=[0,0,0,0], annotation=''):
        #self.key = key
        self.abb = abb
        self.x = x
        self.y = y
        self.z = z
        self.radius = radius
        self.r = rgba[0]
        self.g = rgba[1]
        self.b = rgba[2]
        self.a = rgba[3]
        self.annotation = annotation
    
    
    def print(self):
        print('<%.1f, %.1f, %.1f>, %.1f, <%.1f, %.1f, %.1f, %.1f>'%
        (self.x, self.y, self.z, self.radius, self.r, self.g, self.b, self.a))
    
        
        
#------------------
ms = 1.0 # map_scale
ss = 0.5 # size_scale
cs = 0.01 # color_scale
alpha = 0.3
z = 0

#image size: 1012 x 869
W = 1012
H = 869

emotion_map = np.array([
# abb., x,      y,          z,    radius,     RGBA,                               emotion name,
['A',   424*ms, (H-684)*ms, z*ms,  27*ss*ms,   [ 3.5*cs,  3.9*cs, 97.0*cs, alpha], 'admiration' ],
['B',   333*ms, (H-722)*ms, z*ms,  55*ss*ms,   [14.5*cs,  8.6*cs, 93.7*cs, alpha], 'adoration' ],
['C',   634*ms, (H-562)*ms, z*ms,  62*ss*ms,   [15.0*cs, 78.0*cs, 11.0*cs, alpha], 'apprec' ],
['D',   210*ms, (H-624)*ms, z*ms, 102*ss*ms,   [02.7*cs,  0.4*cs,  5.1*cs, alpha], 'amusement' ],
['E',   431*ms, (H-274)*ms, z*ms,  37*ss*ms,   [82.0*cs, 16.0*cs, 50.0*cs, alpha], 'anger' ],
['F',   214*ms, (H-369)*ms, z*ms,  65*ss*ms,   [98.0*cs, 82.0*cs, 16.0*cs, alpha], 'anxiety' ],
['G',   547*ms, (H-583)*ms, z*ms,  60*ss*ms,   [05.9*cs, 20.0*cs,  2.0*cs, alpha], 'awe' ],
['H',   189*ms, (H-505)*ms, z*ms,  32*ss*ms,   [50.6*cs, 51.0*cs, 96.5*cs, alpha], 'awkwardness' ],
['I',   572*ms, (H-443)*ms, z*ms,  40*ss*ms,   [40.0*cs, 19.6*cs, 14.5*cs, alpha], 'boredom' ],
['J',   689*ms, (H-563)*ms, z*ms,  43*ss*ms,   [18.0*cs, 80.0*cs, 61.2*cs, alpha], 'calmness' ],
['K',   539*ms, (H-369)*ms, z*ms,  35*ss*ms,   [02.7*cs, 40.8*cs, 43.1*cs, alpha], 'confusion' ],
['L',   813*ms, (H-745)*ms, z*ms,  44*ss*ms,   [00.4*cs, 00.0*cs, 29.8*cs, alpha], 'craving' ],
['M',   474*ms, (H-201)*ms, z*ms,  71*ss*ms,   [57.6*cs, 71.4*cs, 39.2*cs, alpha], 'disgust' ],
['N',   369*ms, (H-331)*ms, z*ms,  37*ss*ms,   [86.3*cs, 64.7*cs, 77.3*cs, alpha], 'Empathic Pain' ],
['O',   583*ms, (H-490)*ms, z*ms,  60*ss*ms,   [67.1*cs, 11.8*cs, 90.6*cs, alpha], 'entrancement' ],
['P',   368*ms, (H-482)*ms, z*ms,  48*ss*ms,   [35.3*cs, 09.8*cs, 32.5*cs, alpha], 'excitement' ],
['Q',   249*ms, (H-296)*ms, z*ms,  81*ss*ms,   [66.3*cs,  7.8*cs, 29.4*cs, alpha], 'fear' ],
['R',   326*ms, (H-219)*ms, z*ms,  78*ss*ms,   [95.7*cs, 75.7*cs, 58.0*cs, alpha], 'horror' ],
['S',   441*ms, (H-555)*ms, z*ms,  64*ss*ms,   [00.0*cs, 00.4*cs, 00.0*cs, alpha], 'interest' ],
['T',   394*ms, (H-708)*ms, z*ms,  23*ss*ms,   [00.0*cs, 00.0*cs, 00.8*cs, alpha], 'joy' ],
['U',   872*ms, (H-476)*ms, z*ms,  62*ss*ms,   [77.6*cs, 47.5*cs, 16.1*cs, alpha], 'nostalgia' ],
['V',   300*ms, (H-370)*ms, z*ms,  41*ss*ms,   [42.7*cs, 39.2*cs, 12.2*cs, alpha], 'relief' ],
['W',   869*ms, (H-215)*ms, z*ms,  52*ss*ms,   [49.0*cs, 88.2*cs, 88.2*cs, alpha], 'romance' ],
['X',   456*ms, (H-343)*ms, z*ms,  43*ss*ms,   [62.7*cs, 70.2*cs, 92.2*cs, alpha], 'sadness' ],
['Y',   322*ms, (H-564)*ms, z*ms,  53*ss*ms,   [31.4*cs, 28.2*cs, 33.3*cs, alpha], 'satisfaction' ],
['Z',   730*ms, (H-212)*ms, z*ms,  60*ss*ms,   [42.4*cs,  3.1*cs, 12.5*cs, alpha], 'sexual desire' ],
['-',   354*ms, (H-402)*ms, z*ms,  35*ss*ms,   [00.0*cs, 12.2*cs, 54.5*cs, alpha], 'surprise' ],
])

emotion_map2 = {
#                    abb., x,      y,      z,    radius,      RGBA,                               emotion name,
'exp-x'    :ExpMark('0',     0*ms, (H-  0)*ms, z*ms,   1*ss*ms,   [ 0.0*cs,  0.0*cs,  0.0*cs, alpha], 'x'),
'admire'   :ExpMark('A',   424*ms, (H-684)*ms, z*ms,  27*ss*ms,   [ 3.5*cs,  3.9*cs, 97.0*cs, alpha], 'admiration' ),
'adore'    :ExpMark('B',   333*ms, (H-722)*ms, z*ms,  55*ss*ms,   [14.5*cs,  8.6*cs, 93.7*cs, alpha], 'adoration' ),
'apprec'   :ExpMark('C',   634*ms, (H-562)*ms, z*ms,  62*ss*ms,   [15.0*cs, 78.0*cs, 11.0*cs, alpha], 'apprec' ),
'amuse'    :ExpMark('D',   210*ms, (H-624)*ms, z*ms, 102*ss*ms,   [02.7*cs,  0.4*cs,  5.1*cs, alpha], 'amusement' ),
'anger'    :ExpMark('E',   431*ms, (H-274)*ms, z*ms,  37*ss*ms,   [82.0*cs, 16.0*cs, 50.0*cs, alpha], 'anger' ),
'anxiety'  :ExpMark('F',   214*ms, (H-369)*ms, z*ms,  65*ss*ms,   [98.0*cs, 82.0*cs, 16.0*cs, alpha], 'anxiety' ),
'awe'      :ExpMark('G',   547*ms, (H-583)*ms, z*ms,  60*ss*ms,   [05.9*cs, 20.0*cs,  2.0*cs, alpha], 'awe' ),
'awkward'  :ExpMark('H',   189*ms, (H-505)*ms, z*ms,  32*ss*ms,   [50.6*cs, 51.0*cs, 96.5*cs, alpha], 'awkwardness' ),
'bored'    :ExpMark('I',   572*ms, (H-443)*ms, z*ms,  40*ss*ms,   [40.0*cs, 19.6*cs, 14.5*cs, alpha], 'boredom' ),
'calm'     :ExpMark('J',   689*ms, (H-563)*ms, z*ms,  43*ss*ms,   [18.0*cs, 80.0*cs, 61.2*cs, alpha], 'calmness' ),
'confused' :ExpMark('K',   539*ms, (H-369)*ms, z*ms,  35*ss*ms,   [02.7*cs, 40.8*cs, 43.1*cs, alpha], 'confusion' ),
'crave'    :ExpMark('L',   813*ms, (H-745)*ms, z*ms,  44*ss*ms,   [00.4*cs, 00.0*cs, 29.8*cs, alpha], 'craving' ),
'disgust'  :ExpMark('M',   474*ms, (H-201)*ms, z*ms,  71*ss*ms,   [57.6*cs, 71.4*cs, 39.2*cs, alpha], 'disgust' ),
'pain'     :ExpMark('N',   369*ms, (H-331)*ms, z*ms,  37*ss*ms,   [86.3*cs, 64.7*cs, 77.3*cs, alpha], 'Empathic Pain' ),
'entranced':ExpMark('O',   583*ms, (H-490)*ms, z*ms,  60*ss*ms,   [67.1*cs, 11.8*cs, 90.6*cs, alpha], 'entrancement' ),
'excited'  :ExpMark('P',   368*ms, (H-482)*ms, z*ms,  48*ss*ms,   [35.3*cs, 09.8*cs, 32.5*cs, alpha], 'excitement' ),
'fear'     :ExpMark('Q',   249*ms, (H-296)*ms, z*ms,  81*ss*ms,   [66.3*cs,  7.8*cs, 29.4*cs, alpha], 'fear' ),
'horror'   :ExpMark('R',   326*ms, (H-219)*ms, z*ms,  78*ss*ms,   [95.7*cs, 75.7*cs, 58.0*cs, alpha], 'horror' ),
'interest' :ExpMark('S',   441*ms, (H-555)*ms, z*ms,  64*ss*ms,   [00.0*cs, 00.4*cs, 00.0*cs, alpha], 'interest' ),
'joy'      :ExpMark('T',   394*ms, (H-708)*ms, z*ms,  23*ss*ms,   [00.0*cs, 00.0*cs, 00.8*cs, alpha], 'joy' ),
'nostalgia':ExpMark('U',   872*ms, (H-476)*ms, z*ms,  62*ss*ms,   [77.6*cs, 47.5*cs, 16.1*cs, alpha], 'nostalgia' ),
'relief'   :ExpMark('V',   300*ms, (H-370)*ms, z*ms,  41*ss*ms,   [42.7*cs, 39.2*cs, 12.2*cs, alpha], 'relief' ),
'romance'  :ExpMark('W',   869*ms, (H-215)*ms, z*ms,  52*ss*ms,   [49.0*cs, 88.2*cs, 88.2*cs, alpha], 'romance' ),
'sad'      :ExpMark('X',   456*ms, (H-343)*ms, z*ms,  43*ss*ms,   [62.7*cs, 70.2*cs, 92.2*cs, alpha], 'sadness' ),
'satisfy'  :ExpMark('Y',   322*ms, (H-564)*ms, z*ms,  53*ss*ms,   [31.4*cs, 28.2*cs, 33.3*cs, alpha], 'satisfaction' ),
'sexual'   :ExpMark('Z',   730*ms, (H-212)*ms, z*ms,  60*ss*ms,   [42.4*cs,  3.1*cs, 12.5*cs, alpha], 'sexual desire' ),
'surprise' :ExpMark('-',   354*ms, (H-402)*ms, z*ms,  35*ss*ms,   [00.0*cs, 12.2*cs, 54.5*cs, alpha], 'surprise' ),
}

people_to_color = {
'ppl-x'    :[0.0, 0.0, 0.0, 1.0],
'younger'  :[0.0, 0.0, 0.0, 1.0],
'shion'    :[1.0, 0.0, 0.0, 0.8],
'shya'     :[0.0, 0.0, 0.8, 0.8],
'masahiko' :[0.0, 0.0, 1.0, 0.8],
'masami'   :[0.0, 0.0, 0.8, 0.8], 
'sora'     :[1.0, 1.0, 0.0, 0.8],
'yukari'   :[1.0, 0.0, 1.0, 0.8],
'mori'     :[0.0, 0.0, 0.0, 0.8],
'hiromi':[1.0, 0.5, 0.0, 0.8],
'makoto':[1.0, 0.5, 0.0, 0.8],
'susumu':[0.5, 0.2, 0.0, 0.8],
'kazuko':[0.5, 0.2, 0.0, 0.8], 
'kazuko-boy':[0.0, 0.0, 0.0, 0.8], 
'kazuma':[0.0, 0.0, 0.0, 0.8], 
'grandma':[0.0, 0.0, 0.0, 0.8],
'grandpa':[0.0, 0.0, 0.0, 0.8],
'mama':[0.0, 0.0, 0.0, 0.8], 
'papa':[0.0, 0.0, 0.0, 0.8], 
'yoriko':[0.0, 0.0, 0.0, 0.8], 
'kenji':[0.0, 0.0, 0.0, 0.8], 
'shoko':[0.0, 0.0, 0.0, 0.8], 
'yoko':[0.0, 1.0, 1.0, 0.8], 
'ykma':[0.0, 0.0, 0.0, 0.8], 
'kaoru':[0.0, 0.5, 0.5, 0.8],
'saki':[0.0, 0.0, 0.0, 0.8], 
'tatsumi':[0.0, 0.0, 0.0, 0.8],
'aoi':[0.0, 0.0, 0.0, 0.8],
'ejima':[0.0, 0.0, 0.0, 0.8], 
'ejima-girl':[0.0, 0.0, 0.0, 0.8], 
'akane':[0.0, 0.0, 0.0, 0.8], 
'nishina':[0.0, 0.0, 0.0, 0.8],
'chinatsu':[0.0, 0.0, 0.0, 0.8],
'mai':[0.0, 0.0, 0.0, 0.8],
'asagi':[0.0, 0.0, 0.0, 0.8], 
'director':[0.0, 0.0, 0.0, 0.8],
'cameraman':[0.0, 0.0, 0.0, 0.8],
'reiko':[0.0, 0.0, 0.0, 0.8],
'matsu':[0.0, 0.0, 0.0, 0.8], 
'risa':[0.0, 0.0, 0.0, 0.8],
'mika':[0.0, 0.0, 0.0, 0.8],
'fumiya':[0.0, 0.0, 0.0, 0.8],
'kazuki':[0.0, 0.0, 0.0, 0.8],
'tooru':[0.0, 0.0, 0.0, 0.8],
'te-chan':[0.0, 0.0, 0.0, 0.8],
'kimihiro':[0.0, 0.0, 0.0, 0.8], 
'tomomi':[0.0, 0.0, 0.0, 0.8], 
'kyoko':[0.0, 0.0, 0.0, 0.8],
'aya':[0.0, 0.0, 0.0, 0.8], 
'tenko':[0.0, 0.0, 0.0, 0.8], 
'kenji-ma':[0.0, 0.0, 0.0, 0.8],
'hanzu':[0.0, 0.0, 0.0, 0.8],
'teacher':[0.0, 0.0, 0.0, 0.8],
'villain':[0.0, 0.0, 0.0, 0.8],
'nurse':[0.0, 0.0, 0.0, 0.8],
'club-boss':[0.0, 0.0, 0.0, 0.8],#
'boss':[0.0, 0.0, 0.0, 0.8],     # 
'police':[0.0, 0.0, 0.0, 0.8],   # 警察
'player':[0.0, 0.0, 0.0, 0.8],   
'sth':[0.0, 0.0, 0.0, 0.8],      # something

'chairman':[0.0, 0.0, 0.0, 0.8],           
'ryo':[0.0, 0.0, 0.0, 0.8],      
'boy':[0.0, 0.0, 0.0, 0.8],      # A girl is dressing as a boy (to be removed)
'girl':[0.0, 0.0, 0.0, 0.8],     # A boy is dressing as a girl (to be removed)
'old-woman':[0.0, 0.0, 0.0, 0.8],# (to be removed)
'old-man':[0.0, 0.0, 0.0, 0.8],  # (to be removed)
'masahiko-girl' :[0.0, 0.0, 0.0, 0.8],  # Masahiko dressing as a girl
                     # 雅美
#'shion-boy' :[0.0, 0.0, 0.0, 0.8],     # Shion dressing as a boy
#                    # 男装的紫苑
}# end of people_to_color

#-------------------------------
class Adapter(object):
    m_panel_od = None
    
    def __init__(self, m_panel_od):
        self.m_panel_od = m_panel_od
    
    def get_data_to_ui(self):
        data_matrix = []
        rc = len(self.m_panel_od)
        for i in range(rc):
            row = [None]* len(list(fc_tag.name.keys())) # one ExpMark for each people
            data_matrix.append(row)
        
        for index, panel in self.m_panel_od.items():
            i = list(self.m_panel_od.keys()).index(index)
            for ppl in panel.m_peple_list:
                ppl_name = ppl.m_name_list[0]
                j = self.ppl_i(ppl_name)
                exp_list = ppl.get_explist()
                exp_list_trimed = self.exclude_tag_with_prefix_plus(exp_list)
                print('%d, %d, %s, %s'%(i, j, ppl_name, str(exp_list)) )
                if exp_list != exp_list_trimed:    
                    print('          (trimed: %s)'%(str(exp_list_trimed)) )
                em = self.get_average_expmark(exp_list_trimed)
                
                if em is not None:
                    em.r = people_to_color[ppl_name][0]
                    em.g = people_to_color[ppl_name][1]
                    em.b = people_to_color[ppl_name][2]
                    em.a = people_to_color[ppl_name][3]
                
                data_matrix[i][j] = em
                

        for row in data_matrix:
            for e in row:
                if e == None:
                    print('.',end='')
                else:
                    print('!',end='')
            print('\n')

        return data_matrix
        
        
    def get_average_expmark(self, exp_list):
        if len(exp_list) == 0:
            return None
            
        sum_x=sum_y=sum_z=0
        sum_s=0
        sum_r=sum_g=sum_b=sum_a=0
        
        i=0
        for exp in exp_list:
            if exp == 'ppl-x':
                continue
            i += 1
            sum_x += emotion_map2[exp].x
            sum_y += emotion_map2[exp].y
            sum_z += emotion_map2[exp].z
            sum_s += emotion_map2[exp].radius
            sum_r += emotion_map2[exp].r
            sum_g += emotion_map2[exp].g
            sum_b += emotion_map2[exp].b
            sum_a += emotion_map2[exp].a
            
        return ExpMark( abb='', 
                        x=sum_x/i, y=sum_y/i, z=sum_z/i, 
                        radius=sum_s/i, 
                        rgba=[sum_r/i,sum_g/i,sum_b/i,sum_a/i], 
                        annotation=str(exp_list) )
        
        
    def ppl_i(self, ppl_name):
        '''
        'ppl-x' 0, 'younger' 1, 'shion' 2, 'shya' 3, 'masami' 5
        '''
        t = list(fc_tag.name.keys())
        return t.index(ppl_name)
        
        
    def exclude_tag_with_prefix_plus(self, lst):
        ret_lst = []
        for e in lst:
            if e[0] == '+': # ignore +tag
                continue
            elif e[0] == '-':  # trim '-' for -tag
                ret_lst.append(e[1:])
            else:
                ret_lst.append(e)
            
        return ret_lst

        
def show_gui(backend_name, data_matrix):    
    if backend_name == 'matplotlib':
        import fc_tag_plot_matplotlib
        fc_tag_plot_matplotlib.main(emotion_map, data_matrix)
    elif  backend_name == 'plotly':
        pass
    else:
        assert 0, 'unhandled plot backend: <%s>'%(backend_name)
    
    
def main(root_dir, vol_p_close_open_range, backend_name):
    import fc_tag_analysis
    g_plmgr = fc_tag_analysis.main_(root_dir, vol_p_close_open_range)
    # deug
    g_plmgr.print('')
    
    adapter = Adapter(g_plmgr.m_panel_od)
    data_matrix = adapter.get_data_to_ui()
    
    show_gui(backend_name, data_matrix)


if '__main__' == __name__:
    import os
    import util_logging
    
    os.environ['AUTOMATION_LOG_LEVEL'] = 'INFO' #  DEBUG INFO
    util_logging.setup()

    username = os.path.expanduser('~').split('/')[-1]
    
    root_dir = '/media/%s/home/dev/family-compo-asset/family-compo-asset-vol03/v03'%(username)
    #root_dir= '/media/%s/home/tmp5/'%(username),
    #root_dir= '/media/%s/home/tmp'%(username), 
    #root_dir= '/media/%s/home/tmp4/family-compo-asset/'%(username),
    #root_dir= '/media/%s/home/tmp'%(username),   
    
    
    import check_file_name_extension
    check_file_name_extension.main(
    root_dir=root_dir,
    include_filters=['*'],
    exclude_filters=['family-compo-asset-utility', 'family-compo-mods', '.git']
    )    
    
    
    vol_p_close_open_range=['03_000', '03_031']
    #vol_p_close_open_range=['02_192_3', '02_192_4'] # 02_011_5, 02_025_0, 02_034_2, 02_035_0, 02_036_1, 02_037_0, 02_037_3, 02_039_2, 02_081_3, 02_084_4, 02_107_1, 02_148_0, 02_152_0, 02_192_3
    main(root_dir, vol_p_close_open_range, 'matplotlib')
    


