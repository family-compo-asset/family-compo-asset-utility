
import collections
import os
import sys

import util_logging

# let me import fc_tag
script_dir = os.path.dirname(os.path.realpath(os.path.join(os.getcwd(), os.path.expanduser(__file__)) ) )
sys.path.append(os.path.normpath(os.path.join(script_dir, '../family-compo-asset-vol01')))
print('sys.path=', sys.path)
import fc_tag

#
class FileName(object):
    m_filepath = ''
    
    def __init__(self, filepath):
        self.m_filepath = filepath
        
    def get_index(self):
        fn_base = os.path.basename(self.m_filepath)
        #util_logging.info('fn_base: %s', fn_base)

        fn_no_ext = fn_base.split('.')[0]
        #util_logging.info('fn_no_ext: %s', fn_no_ext)

        fn_index = fn_no_ext.split('__')[0]
        #util_logging.info('fn_index: %s', fn_index)    
        
        # e.g. '02_010_3' means this panel is located at 
        # volume 02, page 010, panel 3.
        return fn_index
    
    def is_in_range(self, vol_p_close_open_range):
        fn_index = self.get_index()
        
        # vol_p_close_open_range is [start, end)
        start = vol_p_close_open_range[0]
        end = vol_p_close_open_range[1]

        # compare the strings, note: e.g. 
        # '02_010_0' <= '02_010' is False, 
        # '02_010_0' <  '02_010' is False,
        if (start <= fn_index ) and (fn_index < end):
            if len(fn_index.split('_')) != 3:
                util_logging.warning('>> index invalid: <%s>', fn_index)
                return False
            else:
                return True
        else:
            return False
        
#
class FileListManager(object):
    m_list = None
    
    def __init__(self, filelist):
        self.m_list = filelist
        
    def filter(self, vol_p_range):
        ret = []
        
        for f in self.m_list:
            fn = FileName(f)
            if fn.is_in_range(vol_p_range):
                ret.append(f)

        return ret
    
    def print_list(self):
        util_logging.info('>> FileListManager.m_list, %d found:', len(self.m_list))
        for fp in self.m_list:
            util_logging.info('%s', fp)
        util_logging.info('<< FileListManager.m_list, %d found.', len(self.m_list))
        util_logging.info('\n')
    
#=============================================
# People
class People(object):
    m_name_list = None
    m_dir_list = None     
    m_exp_list = None
    m_act_list = None
    m_drs_list = None
    m_ext_list = None
    
    def __init__(self, tag_group):
        '''
        \@tag_group e.g. yukari_front_calm_surprise_open-door_upper-gray
        '''
        self.m_name_list = []
        self.m_dir_list = []     
        self.m_exp_list = []
        self.m_act_list = []
        self.m_drs_list = []
        self.m_ext_list = []
        
        tag_list = tag_group.split('_')
        for tag in tag_list:
            if tag == '':
                raise RuntimeError( 'empty tag is found. <%s>'%(str(tag_list)) )
            elif '+'==tag[0]: 
                # panel= f(FC_plot)
                # a tag with prefix '+' means that: the plot of F.COMPO has NOT 
                # this tag(e.g. an expression), but this panel CAN BE USED FOR this tag(expression).
                assert len(tag) > 1, 'it should not be a sigle "+"'
                assert len(tag.split('+')) == 2, 'it should not have many "+"'

                # remove '+' and process this tag again
                self.process_one_tag(tag[1:], tag)
                
            elif '-'==tag[0]: 
                # panel = f(FC_plot)
                # a tag with prefix '-' means that: the plot of F.COMPO HAS
                # this tag(e.g. an expression), but this panel does NOT show this tag(expression).
                assert len(tag) > 1, 'it should not be a sigle "-"'
                assert tag[1] != '-', 'it should not have prefix "--"'

                # remove '+' and process this tag again
                self.process_one_tag(tag[1:], tag)
            
            else:
                self.process_one_tag(tag, tag)
        
        
    def process_one_tag(self, tag, tag_with_prefix):
        if tag in fc_tag.name:
            assert tag not in self.m_name_list, 'duplicated tag: <%s> <%s>'%(tag, tag_with_prefix)
            self.m_name_list.append(tag_with_prefix)
            
        elif tag in fc_tag.direction:
            assert tag not in self.m_dir_list, 'duplicated tag: <%s> <%s>'%(tag, tag_with_prefix)
            self.m_dir_list.append(tag_with_prefix)
            
        elif tag in fc_tag.expression:
            assert tag not in self.m_exp_list, 'duplicated tag: <%s> <%s>'%(tag, tag_with_prefix)
            self.m_exp_list.append(tag_with_prefix)
            
        elif tag in fc_tag.action:
            assert tag not in self.m_act_list, 'duplicated tag: <%s> <%s>'%(tag, tag_with_prefix)
            self.m_act_list.append(tag_with_prefix)
        
        # dress ---------------------
        elif tag in fc_tag.dress:
            assert tag not in self.m_drs_list, 'duplicated tag: <%s> <%s>'%(tag, tag_with_prefix)
            self.m_drs_list.append(tag_with_prefix)

        elif '-' in tag:
            w_list = tag.split('-')
            for w in w_list:
                if w in fc_tag.dress:
                    continue #self.m_drs_list.append(w)
                else:
                    raise RuntimeError('(dress)tag is not found: tag=<%s> <%s>, w=<%s>'%(tag, tag_with_prefix, w))
            
            assert tag not in self.m_drs_list, 'duplicated tag: <%s> <%s>'%(tag, tag_with_prefix)
            self.m_drs_list.append(tag_with_prefix) # add the whole tag instead of w
        
        # extension------------------
        elif tag in fc_tag.extension:
            assert tag not in self.m_ext_list, 'duplicated tag: <%s> <%s>'%(tag, tag_with_prefix)
            self.m_ext_list.append(tag_with_prefix)
        # some property can be attached to people as an extension
        elif tag in fc_tag.prop:
            assert tag not in self.m_ext_list, 'duplicated tag: <%s> <%s>'%(tag, tag_with_prefix)
            self.m_ext_list.append(tag_with_prefix)
            
        # ---------------------------
        else:
            raise RuntimeError('unhandled tag <%s> <%s>',tag, tag_with_prefix)
           
            
    def is_me(self, new_obj):
        if new_obj is None:
            return False
            
        assert len(self.m_name_list) > 0
        assert len(new_obj.m_name_list) > 0
        
        i = set(self.m_name_list) # I, me
        o = set(new_obj.m_name_list) # the new/other people
        sd = i.symmetric_difference(o)
        if (len(sd) == 0) or (sd == set(['ppl-x'])):
            return True
        else:
            return False
            
            
    def get_explist(self):
        return self.m_exp_list
    
    
    def merge(self, new_obj):
        if new_obj is None:
            return 
            
        # list(set([...])) is not used, because it may change the order
        diff =  [x for x in new_obj.m_name_list if x not in self.m_name_list]
        self.m_name_list += diff
        
        diff =  [x for x in new_obj.m_dir_list if x not in self.m_dir_list]
        self.m_dir_list += diff

        diff =  [x for x in new_obj.m_exp_list if x not in self.m_exp_list]
        self.m_exp_list += diff
        
        diff =  [x for x in new_obj.m_act_list if x not in self.m_act_list]
        self.m_act_list += diff
        
        diff =  [x for x in new_obj.m_drs_list if x not in self.m_drs_list]
        self.m_drs_list += diff
        
        diff =  [x for x in new_obj.m_ext_list if x not in self.m_ext_list]
        self.m_ext_list += diff
        
        
    def check(self, msg):
        # todo ...
        # check inclusion relationship:
        # e.g. if 'chopstick' exists, then 'dinnerware' should exist too.
        if len(self.m_name_list) == 1:
            if self.m_name_list[0]=='ppl-x':
                if  (not self.does_list_has_data(self.m_dir_list)) and \
                    (not self.does_list_has_data(self.m_exp_list)) and \
                    (not self.does_list_has_data(self.m_act_list)) and \
                    (not self.does_list_has_data(self.m_drs_list)) and \
                    (not self.does_list_has_data(self.m_ext_list)) :
                    assert 0, 'this may be an error. people "ppl-x" has no data at all'
                
                if  (not self.does_list_has_meaningful_data(self.m_dir_list)) and \
                    (not self.does_list_has_meaningful_data(self.m_exp_list)) and \
                    (not self.does_list_has_meaningful_data(self.m_act_list)) and \
                    (not self.does_list_has_meaningful_data(self.m_drs_list)) and \
                    (not self.does_list_has_meaningful_data(self.m_ext_list)) :
                    assert 0, 'this may be an error. people "ppl-x" has no meaningful data'
                    
       
    def does_list_has_data(self, lst):
        return (lst is not None) and (len(lst) > 0)
        
    def does_list_has_meaningful_data(self, lst):
        if not self.does_list_has_data(lst):
            return False
        
        if lst is self.m_dir_list:
            if len(lst) > 0:
                return True
            
        if lst is self.m_exp_list:
            s = set(lst)
            s.discard('exp-x')
            if len(s) > 0:
                return True
                
        if lst is self.m_act_list:
            s = set(lst)
            s.discard('act-x')
            if len(s) > 0:
                return True
                
        if lst is self.m_drs_list:
            s = set(lst)
            s.discard('drs-x')
            if len(s) > 0:
                return True
                
        if lst is self.m_ext_list:
            if len(lst) > 0:
                return True
                
        return False
        
        
    def print(self, msg):
        util_logging.info('>> <%s> name: <%s>', msg, str(self.m_name_list))
        util_logging.info('   <%s>  dir: <%s>', msg, str(self.m_dir_list))
        util_logging.info('   <%s>  exp: <%s>', msg, str(self.m_exp_list))
        util_logging.info('   <%s>  act: <%s>', msg, str(self.m_act_list))
        util_logging.info('   <%s>  drs: <%s>', msg, str(self.m_drs_list))
        util_logging.info('   <%s>  ext: <%s>', msg, str(self.m_ext_list))        

# Scene
class Scene(object):
    m_tag_list = None
    
    def __init__(self, tag_group):
        self.m_tag_list = []      
          
        tag_list = tag_group.split('_')
        for tag in tag_list:
            assert tag in fc_tag.scene, 'it should be a scene tag: <%s>'%(tag)
            assert tag not in self.m_tag_list, 'it should not be duplicated: <%s>'%(tag)
            self.m_tag_list.append(tag)


    def merge(self, new_obj):
        if new_obj is None:
            return 
            
        diff =  [x for x in new_obj.m_tag_list if x not in self.m_tag_list]
        self.m_tag_list += diff
        
        
    def check(self, msg):
        # todo ...
        pass
        
        
    def print(self, msg):
        util_logging.info('>> <%s>  scn: <%s>', msg, str(self.m_tag_list))


# Property
class Property(object):
    m_tag_list = None
    
    def __init__(self, tag_group):
        self.m_tag_list = []
        
        tag_list = tag_group.split('_')
        for tag in tag_list:
            assert tag in fc_tag.prop, 'it should be a property tag: <%s>'%(tag)
            assert tag not in self.m_tag_list, 'it should not be duplicated: <%s>'%(tag)
            self.m_tag_list.append(tag)
        
        
    def merge(self, new_obj):
        if new_obj is None:
            return 
            
        diff =  [x for x in new_obj.m_tag_list if x not in self.m_tag_list]
        self.m_tag_list += diff
        
        
    def check(self, msg):
        # todo ...
        pass
        
        
    def print(self, msg):
        util_logging.info('>> <%s> prop: <%s>', msg, str(self.m_tag_list))

# Effect
class Effect(object):
    m_tag_list = None
    
    def __init__(self, tag_group):
        self.m_tag_list = []
        
        tag_list = tag_group.split('_')
        for tag in tag_list:
            assert tag in fc_tag.effect, 'it should be an effect tag: <%s>'%(tag)
            assert tag not in self.m_tag_list, 'it should not be duplicated: <%s>'%(tag)
            self.m_tag_list.append(tag)
            
        
    def merge(self, new_obj):
        if new_obj is None:
            return 
            
        diff =  [x for x in new_obj.m_tag_list if x not in self.m_tag_list]
        self.m_tag_list += diff
        
        
    def check(self, msg):
        # todo ...
        pass
        
        
    def print(self, msg):
        util_logging.info('>> <%s>   fx: <%s>', msg, str(self.m_tag_list))
        
        
# Panel
class Panel(object):
    m_index = None # e.g. 01_023_4
    m_name_list = None # filename_base without extention
    m_peple_list = None
    m_scene = None
    m_prop = None
    m_effect = None
    
    def __init__(self, filename_base_no_ext):
        self.m_index = ''
        self.m_name_list = []
        self.m_peple_list = []

        # panel name
        self.m_name_list.append(filename_base_no_ext)
        
        t = filename_base_no_ext.split('__')
        self.m_index = t[0]  # index
        tag_group_list = t[1:]  # e.g. ['yukari_front_calm_open-door_head', 'inner_day_scn-x_diningroom']
        for tag_group in tag_group_list:
            self.create_tag_type(tag_group)
            
        self.check('check in the __init__()')
        
    def create_tag_type(self, tag_group):
        '''
        \@tag_group   e.g. yukari_front_calm_surprise_open-door_upper-gray
        '''
        tag_list = tag_group.split('_')
        try:
            if tag_list[0] in fc_tag.name:
                # new people
                new_ppl = People(tag_group) 
                
                # if I have this new people, then merge this people with the new data
                megered_flag = False
                for old_ppl in self.m_peple_list:
                    if old_ppl.is_me(new_ppl):
                        old_ppl.merge(new_ppl)
                        megered_flag = True
                
                # if I don't have this new people, then append it.
                if False == megered_flag:
                    self.m_peple_list.append( new_ppl )  
                    
            elif tag_list[0] in fc_tag.scene:
                #self.m_scene.print('old scene: ')
                #util_logging.warning('>> new scene: <%s>', tag_group)
                assert self.m_scene is None, 'in a panel, old scene should NOT be erased. old:<%s>, new:<%s>'%(str(self.m_scene.m_tag_list), str(tag_list))
                self.m_scene = Scene(tag_group)
                
            elif tag_list[0] in fc_tag.prop:
                #self.m_prop.print('old property: ')
                #util_logging.warning('>> new property: <%s>', tag_group)
                assert self.m_prop is None, 'in a panel, old property should NOT be erased. old:<%s>, new:<%s>'%(str(self.m_prop.m_tag_list), str(tag_list))
                self.m_prop = Property(tag_group)
                
            elif tag_list[0] in fc_tag.effect:
                #self.m_effect.print('old effect: ')
                #util_logging.warning('>> new effect: <%s>', tag_group)
                assert self.m_effect is None, 'in a panel, old effect should NOT be erased. old:<%s>, new:<%s>'%(str(self.m_effect.m_tag_list), str(tag_list))
                self.m_effect = Effect(tag_group)
                
            else:
                raise RuntimeError('unhandled tag group <%s>, <%s>', tag_group, str(tag_list))
        except:
            util_logging.error('>> <%s>', str(self.m_name_list))
            raise
        

    def does_it_match_my_index(self, panel_obj):
        return panel_obj.get_index() == self.m_index
        
        
    def get_index(self):
        return self.m_index
        
    def has_multi_panel(self):
        return len(self.m_name_list) > 1
        
        
    def merge(self, new_panel):
        # merge name list
        diff =  [x for x in new_panel.m_name_list if x not in self.m_name_list]
        self.m_name_list += diff
        
        # index doesn't change 
        #self.m_index = 
        
        #new_panel.print('Debug| before merge...from')
        #self.print('Debug| before merge...to')
        
        # merge people list
        # 1. update people that self.m_peple_list already has
        for this in self.m_peple_list:
            for other in new_panel.m_peple_list:
                if this.is_me(other):
                    this.merge(other)
        #self.print('Debug| after merge step 1')   
                 
        # 2. add new people that self.m_peple_list doesn't have
        new_people_list = []
        for other in new_panel.m_peple_list:
            other_is_new = True
            for this in self.m_peple_list:
                if this.is_me(other):
                    other_is_new = False
            if True == other_is_new: # $other is new for my people list
                new_people_list.append(other)
        
        self.m_peple_list += new_people_list
        #self.print('Debug| after merge step 2') 
                    
        # merge scene list
        if self.m_scene is None:
            self.m_scene = new_panel.m_scene
        else:
            self.m_scene.merge(new_panel.m_scene)
        
        # merge property list
        if self.m_prop is None:
            self.m_prop = new_panel.m_prop
        else:
            self.m_prop.merge(new_panel.m_prop)
     
        # merge effect list
        if self.m_effect is None:
            self.m_effect = new_panel.m_effect
        else:
            self.m_effect.merge(new_panel.m_effect)
            
        self.check('check after merge.')
        
    def check(self, msg):
        util_logging.info('>> <%s>, <%s>:', msg, '\n'.join(self.m_name_list))   
        
        # -
        util_logging.info('>> <%s>, check every people <%d>:', msg, len(self.m_peple_list))
        for p in self.m_peple_list:
            p.check(msg)
        
        # -
        util_logging.info('>> <%s>, check duplicated people <%d>:', msg, len(self.m_peple_list))
        len_ppl = len(self.m_peple_list)
        for i in range(len_ppl):
            for j in range(len_ppl):
                if j == i:
                    continue
                
                pi = self.m_peple_list[i]
                pj = self.m_peple_list[j]
                if pi.is_me(pj):
                    raise RuntimeError('People should not be duplcated. <%s>, <%s>'%( str(pi.m_name_list), str(pj.m_name_list)) )
                '''
                # Now, i, j are different people_object
                i_name_set = set(self.m_peple_list[i].m_name_list)
                j_name_set = set(self.m_peple_list[j].m_name_list)
                intersection = i_name_set.intersection(j_name_set)
                if len(intersection) > 0:
                    intersection.discard('ppl-x')
                    intersection.discard('younger')
                    if len(intersection) > 0:
                        raise RuntimeError('People should not be duplcated. <%s>, <%s>'%( str(i_name_set), str(j_name_set)) )
                '''
                    
        #
        if self.m_scene is not None:
            self.m_scene.check(msg)
        #
        if self.m_prop is not None:
            self.m_prop.check(msg)
        #
        if self.m_effect is not None:
            self.m_effect.check(msg)
        
        util_logging.info('<< <%s>, check ends. ', msg)
        #util_logging.info('\n')

    def check_for_multi_panel(self, msg):
        # -
        util_logging.info('>> <%s>, check that people are listed in one single panel at least once', msg)
        all_people_names_in_this_panel = set()
        for ppl in self.m_peple_list:
            all_people_names_in_this_panel.update(ppl.m_name_list) # add list's elements to set
        
        issubset = False
        for panel in self.m_name_list:
            count_in = 0
            
            for ppl in all_people_names_in_this_panel:
                if ppl in panel:
                    count_in += 1
                    
            if count_in == len(all_people_names_in_this_panel):
                issubset = True
                break;
                
        if issubset == False:
            assert 0, 'people should be listed in one single panel at least once. <\n%s\n>, \nall_people_names_in_this_panel:<%s>'%('\n'.join(self.m_name_list), str(all_people_names_in_this_panel))
            
    
    def print(self, msg):
        util_logging.info('>> <%s>,  panel <%s>:', msg, self.m_index)
        util_logging.info('   <%s>,        <%s>:', msg, '\n'.join(self.m_name_list))
        
        util_logging.info('>> <%s>, people <%d>:', msg, len(self.m_peple_list))
        for p in self.m_peple_list:
            p.print(msg)
            
        if self.m_scene is not None:
            self.m_scene.print(msg)
        
        if self.m_prop is not None:
            self.m_prop.print(msg)
            
        if self.m_effect is not None:
            self.m_effect.print(msg)
        
        util_logging.info('<< <%s>', msg)
        #util_logging.info('\n')
        
        
        
# PanelListManager
class PanelListManager(object):
    m_panel_od = None # ordered dict for panels
    
    def __init__(self):
        self.m_panel_od = collections.OrderedDict()
        
        
    def update(self, panel_obj):
        i = panel_obj.get_index()
        if i in self.m_panel_od:
            #panel_obj.print('to merge from this panel...')
            # update the existing panel
            self.m_panel_od[i].merge(panel_obj)
        else:
            # create a new panel
            self.m_panel_od[i] = panel_obj

    
    def check(self, msg):
        util_logging.info('>> Check panel list, %d found:', len(self.m_panel_od))
        for index, panel in self.m_panel_od.items():
            panel.check(msg)
        util_logging.info('<< Check panel list, %d found.', len(self.m_panel_od))
        util_logging.info('\n')
        
        util_logging.info('>> Check multi panel list, %d found:', len(self.m_panel_od))
        for index, panel in self.m_panel_od.items():
            if panel.has_multi_panel():
                panel.check_for_multi_panel(msg)
        util_logging.info('<< Check multi panel list, %d found.', len(self.m_panel_od))
        util_logging.info('\n')                

                    

    def print(self, msg):
        util_logging.info('>> panel list, %d found:', len(self.m_panel_od))
        for index, panel in self.m_panel_od.items():
            panel.print('%s'%(index))
        util_logging.info('<< panel list, %d found.', len(self.m_panel_od))
        util_logging.info('\n')
        


