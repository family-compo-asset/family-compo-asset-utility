'''
cd /home/dev/family-compo-asset/family-compo-asset-utility
python3 ./fc_tag_statistics.py 

'''
import errno
import glob
import os
import shutil
import sys

import util_logging
import util_common

# let me import fc_tag
script_dir = os.path.dirname(os.path.realpath(os.path.join(os.getcwd(), os.path.expanduser(__file__)) ) )
sys.path.append(os.path.normpath(os.path.join(script_dir, '../family-compo-asset-vol01')))
print('sys.path=', sys.path)
import fc_tag

def glob_filter(source_dir, include_filters, exclude_filters=None):
    '''
    glob_filter('/a/b/c', '/d/e/f', ['*.h'], [])
    glob_filter('/a/b/c', '/d/e/f', ['*.h', '*.py'], [])
    glob_filter('/a/b/c', '/d/e/f', ['*'], '*.pyc')
    glob_filter('/a/b/c', '/d/e/f', '*', ['*.pyc', '*.log'])
    '''
    if include_filters is None:
        include_filters = []

    if exclude_filters is None:
        exclude_filters = []

    include_files = []
    for filter_ in include_filters:
         include_files += glob.iglob(os.path.join(source_dir, filter_))
    #log('include_files=%s', ' '.join(include_files))

    exclude_files = []
    for filter_ in exclude_filters:
        exclude_files += glob.iglob(os.path.join(source_dir, filter_))
    #log('exclude_files=%s', ' '.join(exclude_files))

    files = list(set(include_files) - set(exclude_files))
    #log('files=%s', ' '.join(files))
    return files

def get_immediate_subdirs(a_dir):
    sub_archives = []
    try:
        sub_archives = os.listdir(a_dir)
    except OSError as e:
        context = ' os.listdir(%s). '%a_dir
        if e.errno == errno.ENOENT:
            raise RuntimeError('No such file or directory. ' + context)
        else:
            raise RuntimeError('Unhandled exception: %s, %s: (%s).', errno.errorcode[e.errno], e.strerror, context)

    return [name for name in sub_archives if os.path.isdir(a_dir+'/'+name)]


def test_tag_statistics(root_dir, include_filters, exclude_filters):
    util_logging.info('>> test_tag_statistics(%s, )', root_dir)

    root_dir = root_dir.replace('\\','/')
    
    if root_dir[-1] != '/':
        root_dir += '/'
    
    filepath_list = glob_filter(root_dir, include_filters, exclude_filters)
    filepath_list.sort()

    util_logging.info('>> %d found:', len(filepath_list))
    #for filepath in filepath_list:
    #    util_logging.info('%s', filepath)
    util_logging.info('<< %d found.', len(filepath_list))
    util_logging.info('\n')
    for filepath in filepath_list:
        do_statistics_on_one_imagefile(filepath)

    # process subdirectories
    #subdirs = get_immediate_subdirs(root_dir)
    sub_archive_list = glob.glob(os.path.join(root_dir, '*'))
    for sub_archive in sub_archive_list:
        if sub_archive in [os.path.join(root_dir, exclude_filter) for exclude_filter in exclude_filters ]:
            util_logging.info('exclude: %s', sub_archive)
            continue
        if os.path.isdir(sub_archive):
            test_tag_statistics(sub_archive, include_filters, exclude_filters)

def do_statistics_on_one_tag(tag, fn_base):
    if tag == '':
        return
        
    elif tag in fc_tag.name:
        fc_tag.name[tag] += 1
        
    elif tag in fc_tag.direction:
        fc_tag.direction[tag] += 1
        
    elif tag in fc_tag.expression:
        fc_tag.expression[tag] += 1
        
    elif tag in fc_tag.action:
        fc_tag.action[tag] += 1
        
    elif tag in fc_tag.dress:
        fc_tag.dress[tag] += 1
        
    elif tag in fc_tag.extension:
        fc_tag.extension[tag] += 1
        
    elif tag in fc_tag.scene:
        fc_tag.scene[tag] += 1
        
    elif tag in fc_tag.prop:
        fc_tag.prop[tag] += 1
        
    elif tag in fc_tag.effect:
        fc_tag.effect[tag] += 1
        
    elif '+'==tag[0]: 
        # panel= f(FC_plot)
        # a tag with prefix '+' means that: the plot of F.COMPO has NOT 
        # this tag(e.g. an expression), but this panel CAN BE USED FOR this tag(expression).
        
        # remove '+' and process this tag again
        do_statistics_on_one_tag(tag[1:], fn_base)
        
    elif '-'==tag[0]: 
        # panel = f(FC_plot)
        # a tag with prefix '-' means that: the plot of F.COMPO HAS
        # this tag(e.g. an expression), but this panel does NOT show this tag(expression).
        
        # remove '+' and process this tag again
        do_statistics_on_one_tag(tag[1:], fn_base)
        
    elif '-' in tag:
        w_list = tag.split('-')
        for w in w_list:
            if w in fc_tag.dress:
                fc_tag.dress[w] += 1
            else:
                raise RuntimeError('(dress)tag is not found: tag=<%s>, w=<%s>, fn_base=<%s>'%(tag, w, fn_base))

    else:
        raise RuntimeError('tag is not found: tag=<%s>, fn_base=<%s>'%(tag, fn_base))
    
def do_statistics_on_one_imagefile(filepath):
    fn_base = os.path.basename(filepath)
    #util_logging.info('fn_base: %s', fn_base)

    fn_no_ext = fn_base.split('.')[0]
    #util_logging.info('fn_no_ext: %s', fn_no_ext)

    fn_no_index = fn_no_ext.split('__')[1:]
    #util_logging.info('fn_no_index: %s', fn_no_index)

    tag_group_list = fn_no_index
    for tag_group in tag_group_list:
        tag_list = tag_group.split('_')
        for tag in tag_list:
            do_statistics_on_one_tag(tag, fn_base)
            

def dict_to_string(d, msg):
    ret_str = ''
    
    ret_str += msg
    
    import operator 
    cd = sorted(d.items(), key=operator.itemgetter(1), reverse=True )
    
    import pprint    
    # 1. output without a format
    #print('pprint.pprint-----------------')
    #pprint.pprint(cd) 
    
    # 2. output with a format
    # 2.1 with pformat
    # output to a string with pprint format
    ret_str += pprint.pformat(cd) 
    # 2.2 with json format
    #import json
    #ret_str += json.dumps(cd, indent=4)
        
    return ret_str

def output_statistics_result(file_path=''):
    util_logging.info('>> output_statistics_result(%s, )', file_path)

    file_path = file_path.replace('\\','/')
        
    str_o = '' # string to be output
    
    str_o += dict_to_string(fc_tag.name, '\nname-------------------------\n')
    str_o += dict_to_string(fc_tag.direction, '\ndirection-------------------------\n')
    str_o += dict_to_string(fc_tag.expression, '\nexpression-------------------------\n')
    str_o += dict_to_string(fc_tag.action, '\naction-------------------------\n')
    str_o += dict_to_string(fc_tag.dress, '\ndress-------------------------\n')
    str_o += dict_to_string(fc_tag.extension, '\nextension-------------------------\n')
    str_o += dict_to_string(fc_tag.scene, '\nscene-------------------------\n')
    str_o += dict_to_string(fc_tag.prop, '\nprop-------------------------\n')
    str_o += dict_to_string(fc_tag.effect, '\neffect-------------------------\n')

    print('STATISTICS RESULT-----------------')
    
    # output to the terminal
    #print(str_o)  
    
    # output to a file
    with open(file_path, 'w') as f:
        f.write(str_o)

    
def main(root_dir, include_filters, exclude_filters):
    print('Statistics. Start >>>')
    
    test_tag_statistics(
    root_dir=root_dir,
    #root_dir='/media/%s/home/tmp'%(username),
    include_filters=include_filters,
    exclude_filters=exclude_filters
    )

    output_statistics_result(file_path='/media/%s/home/dev/family-compo-asset/family-compo-asset-vol01/fc_tag_statistics.txt'%(username))
    
    print('<<< End. Statistics')    
    
if '__main__' == __name__:
    os.environ['AUTOMATION_LOG_LEVEL'] = 'INFO'
    util_logging.setup()

    username = os.path.expanduser('~').split('/')[-1]

    import check_file_name_extension
    check_file_name_extension.main(
    #root_dir='/media/%s/home/tmp5/'%(username),
    root_dir='/media/%s/home/dev/family-compo-asset/'%(username),
    #root_dir='/media/%s/home/tmp'%(username),
    include_filters=['*'],
    exclude_filters=['family-compo-asset-utility', 'family-compo-mods', '.git']
    )
    
    main(
    root_dir='/media/%s/home/dev/family-compo-asset/'%(username),
    #root_dir='/media/%s/home/tmp'%(username),
    include_filters=['*.jpg', '*.png'],
    exclude_filters=['family-compo-asset-utility', 'family-compo-mods', '.git']
    )

