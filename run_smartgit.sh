
# copy this file to ~ or /usb, then run it
export HOME=$PWD
#export HOME=$HOME

echo HOME=$HOME  # $HOME should be /dev

if [[ $HOME == *" "* ]]; then
    echo "MyError: HOME path contains whitespace: " $HOME
fi

# if XDG_CONFIG_HOME defined,
# then $XDG_CONFIG_HOME/smartgit/ is used
# else ~/.config is used, so ~/.config/smartgit/ is used
export XDG_CONFIG_HOME=$HOME/.config
echo XDG_CONFIG_HOME=$XDG_CONFIG_HOME

if [[ $XDG_CONFIG_HOME == *" "* ]]; then
    echo "MyError: XDG_CONFIG_HOME path contains whitespace: " $XDG_CONFIG_HOME
fi

# . ./.profile  # this will run '. ./.bashrc'
FILE=./smartgit/bin/smartgit.sh
if test -f "$FILE"; then
    ./smartgit/bin/smartgit.sh
else
    echo "File not exist: "$File
fi



