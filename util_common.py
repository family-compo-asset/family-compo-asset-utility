import os
import sys
import errno
import util_logging

def tryToResetDir(directory):
    import shutil
    
    # remove the directory if it exists
    if os.path.exists(directory):
        util_logging.debug('remove directory: %s', directory)
        try:
            shutil.rmtree(directory)
        except Exception as e:
            context = ' shutil.rmtree(%s). '%directory
            if e.errno == errno.EACCES:
                tips = '\nTips: Maybe you do not have the permission to remove that directory, ' \
                       '\nor you have open file(s) in that directory, ' \
                       '\nor the directory is open. '
                raise RuntimeError('Permission denied. '+context + tips)
            elif e.errno == errno.ENOTEMPTY:
                tips = '\nTips: Maybe you should leave that directory or close files in that directory. '
                raise RuntimeError('Directory not empty. ' + context + tips)
            else:
                raise RuntimeError('Unhandled exception: %s, %s: (%s).', errno.errorcode[e.errno], e.strerror, context)
        except:
            util_logging.fatal('Unexcepted error when calling shutil.rmtree(%s)', directory)
            raise RuntimeError('How can we get here?')
    
    # create this directory
    try:
        util_logging.debug('create directory: %s', directory)
        os.makedirs(directory)
    except OSError as e:
        context = ' os.makedirs(%s). '%directory
        if e.errno == errno.EACCES:
            tips = '\nTips: Maybe you do not have the permission to remove that directory, ' \
                   '\nor you have open file(s) in that directory, ' \
                   '\nor the directory is open. '
            raise RuntimeError('Permission denied. ' + context + tips)
        else:
            raise RuntimeError('Unhandled exception: %s, %s: (%s).', errno.errorcode[e.errno], e.strerror, context)

