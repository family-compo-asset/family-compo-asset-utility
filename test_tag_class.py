
import fc_tag_class

def test():
    '''
    index = fc_tag_class.Index(1, 3, 0)
    print(index.output())
    scene = fc_tag_class.Scene(['day'])
    print(scene.output())
    prop = fc_tag_class.Property(['train','passerby','sky'])
    print(prop.output())
    filename = fc_tag_class.FileName(i=index, pl=None, s=scene, p=prop, e=None)
    print(filename.output())
    
    index = fc_tag_class.Index(1, 3, 1)
    scene = fc_tag_class.Scene(['day'])
    #print(scene.output())
    prop = fc_tag_class.Property(['train', 'railway','pole','tree','building','logo','sky'])
    #print(prop.output())
    filename = fc_tag_class.FileName(i=index, pl=None, s=scene, p=prop, e=None)
    print(filename.output())

    
    
    index = fc_tag_class.Index(1, 3, 2)
    masahiko = fc_tag_class.People( n='masahiko', 
                                    dir_l=['back','front'], 
                                    exp_l=['nostalgia', 'exp-x'], 
                                    act_l=[], 
                                    drs_l=[fc_tag_class.Dress('upper','long','white')], 
                                    ext=None)
    #print(masahiko.output())
    ppl0 = fc_tag_class.People( n='ppl-x', 
                                dir_l=['front'], 
                                exp_l=['joy', 'amuse'], 
                                act_l=[], 
                                drs_l=[fc_tag_class.Dress('upper','short','white'), 
                                       fc_tag_class.Dress('lower','long','skirt')], 
                                ext=None)
    #print(ppl0.output())
    scene = fc_tag_class.Scene(['day'])
    #print(scene.output())
    prop = fc_tag_class.Property(['bag', 'basket','cloud','sky'])
    #print(prop.output())
    filename = fc_tag_class.FileName(i=index, pl=[masahiko, ppl0], s=scene, p=prop, e=None)
    print(filename.output())
    '''  
     
    index = fc_tag_class.Index(1, 3, 4)
    masahiko = fc_tag_class.People( n='masahiko', 
                                    dir_l=['back','side','h-front', 'h-side'], 
                                    exp_l=['nostalgia', 'exp-x'], 
                                    act_l=[], 
                                    drs_l=[fc_tag_class.Dress('upper','long','white', 'gray'),
                                           fc_tag_class.Dress('lower','long','gray') ], 
                                    ext=None)
    #print(masahiko.output())
    ppl0 = fc_tag_class.People( n='ppl-x', 
                                dir_l=['back','h-back'], 
                                exp_l=['exp-x'], 
                                act_l=['act-x'], 
                                drs_l=[fc_tag_class.Dress('upper','short','white'), 
                                       fc_tag_class.Dress('lower','long','skirt')], 
                                ext=None)
    masahiko.set_part(['upper', 'lower'])
    ppl0.set_part(['upper', 'lower'])
    #print(ppl0.output())
    scene = fc_tag_class.Scene(['day'])
    #print(scene.output())
    prop = fc_tag_class.Property(['bag'])
    #print(prop.output())
    filename = fc_tag_class.FileName(i=index, pl=[ ppl0], s=scene, p=prop, e=None)
    print(filename.output())
    
    
    #filename = 'v01_003_1__people__day__train_railway_pole_tree_building_logo_sky__effect.jpg'
    print('split: %s' %(filename.output().split('.')) )
    
test()
