
import fc_tag

class Index(object):
    volume = 0
    page = 0
    square = 0
    
    def __init__(self, v, p, s):
        self.volume = v
        self.page = p
        self.square = s
        
    def output(self):
        return 'v%02d_%03d_%d'%(self.volume, self.page, self.square)
        
#====================================================================
class Name(object):
    v = ''
    def __init__(self, n):
        # check
        if n not in fc_tag.name:
            raise RuntimeError('<%s> is not found in tag name'%(n))
        
        self.v = n
        
    def output(self):
        return self.v
#
class Direction(object):
    _list = None
    def __init__(self, l=None):
        # check
        for v in l:
            if v not in fc_tag.direction:
                raise RuntimeError('<%s> is not found in tag direction'%(v))
        
        self._list = l
        
    def output(self):
        return '_'.join(self._list)
    
    def has_this_dir(self, direction):
        return direction in self._list
#
class Expression(object):
    _list = None
    def __init__(self, l=None):
        # check
        for v in l:
            if v not in fc_tag.expression:
                raise RuntimeError('<%s> is not found in tag expression'%(v))
        
        self._list = l
        
    def output(self, direction_list):
        ret = '_'.join(self._list)
        if direction_list.has_this_dir('h-back'):
            ret = 'exp-x'
        return ret
#
class Action(object):
    _list = None
    def __init__(self, l=None):
        # check
        for v in l:
            if v not in fc_tag.action:
                raise RuntimeError('<%s> is not found in tag action'%(v))
        
        self._list = l
        
    def output(self):
        return '_'.join(self._list)
#
class Dress(object):
    u_l = ''
    l_s = ''
    out_c = ''
    mid_c = ''
    in_c  = ''
    ext = []
    
    def __init__(self, u_l='', l_s='', out_c='', mid_c='', in_c='', ext=[]):
        # check
        if u_l != '' and u_l not in fc_tag.dress:
            raise RuntimeError('<%s> is not found in tag dress'%(u_l))
        if l_s != '' and l_s not in fc_tag.dress:
            raise RuntimeError('<%s> is not found in tag dress'%(l_s))   
        if out_c != '' and out_c not in fc_tag.dress:
            raise RuntimeError('<%s> is not found in tag dress'%(out_c))
        if mid_c != '' and mid_c not in fc_tag.dress:
            raise RuntimeError('<%s> is not found in tag dress'%(mid_c))
        if in_c != '' and in_c not in fc_tag.dress:
            raise RuntimeError('<%s> is not found in tag dress'%(in_c))
        if ext != []:
            for e in ext:
                if e not in fc_tag.dress:
                    raise RuntimeError('<%s> is not found in tag dress'%(e))
            
        self.u_l = u_l
        self.l_s = l_s
        self.out_c = out_c
        self.mid_c = mid_c
        self.in_c = in_c                
        self.ext = ext
        
    def output(self, part_list, direction_list):
        #print('direction_list=', direction_list)
        ret = ''
        if self.u_l in part_list:
            ret += self.u_l+'-'+self.l_s+'-'
        
            if  direction_list.has_this_dir('back'):
                ret += self.out_c+'-'
            if  direction_list.has_this_dir('side'):
                ret += self.mid_c+'-'
            if  direction_list.has_this_dir('front'):
                ret += self.in_c+'-'
            
        ret += '-'.join(self.ext)
        return ret
            
#
class Extension(object):
    v = ''
    def __init__(self, ext=None):
        if ext is None:
            self.v = ''
            return 
            
        # check
        if ext not in fc_tag.extension:
            raise RuntimeError('<%s> is not found in tag extension'%(ext))
        
        self.v = ext
        
    def output(self):
        return self.v
#
class People(object):
    name = None
    direction_list = []
    expression_list = []
    action_list = []
    dress_list = []
    extension = None
    
    part_list = ['head', 'upper', 'lower']
        
    def __init__(self, n=None, dir_l=None, exp_l=None, act_l=None, drs_l=None, ext=None):
        self.name = Name(n)
        self.direction_list = Direction(dir_l)
        self.expression_list = Expression(exp_l)
        self.action_list = Action(act_l)
        self.dress_list = drs_l
        
        self.extension = Extension(ext)  

    def set_part(self, part_list):
        self.part_list = part_list
    
    def output(self):
        assert self.name is not None
        name_str = self.name.output()
        
        direction_str = ''
        if self.direction_list is not None:
            direction_str = self.direction_list.output()
        
        expression_str = ''
        if self.expression_list is not None:
            expression_str = self.expression_list.output(self.direction_list)
            
        action_str = ''
        if self.action_list is not None:
            action_str = self.action_list.output()
            
        dress_str = ''
        if self.dress_list is not None:
            for drs in self.dress_list:
                dress_str += drs.output(self.part_list, self.direction_list)
                
        ext_str = ''
        if self.extension is not None:
            ext_str = self.extension.output()
            
        ret = '%s_%s_%s_%s_%s_%s'%(name_str, direction_str, expression_str, action_str, dress_str, ext_str)
        return ret
#====================================================================
class Scene(object):
    value_list = None
    
    def __init__(self, vl):
        # check
        for v in vl:
            if v not in fc_tag.scene:
                raise RuntimeError('<%s> is not found in tag scene'%(v))
        
        self.value_list = vl
        
    def output(self):
        return '_'.join(self.value_list)
#
class Property(object):
    value_list = None
    
    def __init__(self, vl):
        # check
        for v in vl:
            if v not in fc_tag.prop:
                raise RuntimeError('<%s> is not found in tag prop'%(v))
        
        self.value_list = vl
        
    def output(self):
        return '_'.join(self.value_list)
#
class Effect(object):
    def __init__(self):
        pass
        
    def output(self):
        pass
#
class FileName(object):
    index = None
    people_list = None
    scene = None
    prop = None
    effect = None
    
    def __init__(self, i=None, pl=None, s=None, p=None, e=None):
        self.index = i
        self.people_list=pl
        self.scene = s
        self.prop = p
        self.effect = e
        
    def output(self):
        assert self.index is not None
        index_str = self.index.output()
        
        people_str = ''
        for ppl in self.people_list:
            #print(ppl.output())
            people_str += ppl.output()+'.'
        
        scene_str = ''
        if self.scene is not None:
            scene_str = self.scene.output()
            
        prop_str = ''
        if self.prop is not None:
            prop_str = self.prop.output()
            
        effect_str = ''
        if self.effect is not None:
            effect_str = self.effect.output()      
            
        ret = '%s.%s.%s.%s.%s'%(index_str, people_str, scene_str, prop_str, effect_str)
        if len(ret) >= 255:
            raise RuntimeError('File Name is <%d>, too long: %s'%(len(ret), ret))
        return ret
        
    
